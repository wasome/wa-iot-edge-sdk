# -*- coding: utf-8 -*-

# Copyright (C) 2017 Intel Corporation.  All rights reserved.
# Copyright (c) WanSheng Intelligent Corp. All rights reserved.

#
# Licensed under the Apache License, Version 2.0 (the "License");
#


from ..model.constants import MediaTypeFormat
from ..model.property_data import PropertyData

class ResourceDataBase(object):
    def __init__(self, raw_payload, fmt):
        self.__raw_payload = raw_payload
        self.__format = fmt
        
        self.is_parsed = False
        self.resource_id = None
        self.items = []


    def get_format(self):
        return self.__format

    def is_parsed(self):
        return self.is_parsed

    def get_raw_payload(self):
        return self.__raw_payload
    


    def get_property_value(self, property_name):
        for data in self.items:
            if data.prop_name == property_name:
                return data.value
        return None

    def set_property_value(self, property_name, value):
        for data in self.items:
            if data.prop_name == property_name:
                data.value = value
                return True
        
        pt = PropertyData(property_name, value)
        self.items.append(pt)
    
    def get_properties(self):
        return self.items

    
    def pack(self):
        return self.__format, self.__raw_payload
