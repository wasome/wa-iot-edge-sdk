# -*- coding: utf-8 -*-

# Copyright (C) 2017 Intel Corporation.  All rights reserved.
# Copyright (c) WanSheng Intelligent Corp. All rights reserved.

#
# Licensed under the Apache License, Version 2.0 (the "License");
#


import time
from enum import Enum

class ValueType(Enum):
    UNKNOWN = 0
    FLOAT = 1
    INT = 2
    STRING = 3
    BOOL = 4

def parse_value(value_string, value_type):
    if(value_type == ValueType.FLOAT):
        return float(value_string)
    elif(value_type == ValueType.INT):
        return int(value_string)
    elif(value_type == ValueType.STRING):
        return value_string
    elif(value_type == ValueType.BOOL):
        return bool(value_string)
    else:
        return None

class PropertyData(object):
    def __init__(self, name, value, value_type = ValueType.UNKNOWN, report_time = None):
        self.prop_name = name
        self.value = value
        self.type = value_type
        self.createtime = int(time.time())
        if report_time is not None:
            self.t = report_time
    
    def set_time(self, t):
        self.t = t

    def describe(self):
        if self.t is not None:
             str_time = ', data time:' + str(self.createtime) + ' ' + time.ctime(self.createtime)
        else:
            str_time = ', create time:' + str(self.createtime) + ' ' + time.ctime(self.createtime)
        return "name:" + self.prop_name + ', value: ' + str(self.value) + ', type: ' + str(self.type) + str_time







