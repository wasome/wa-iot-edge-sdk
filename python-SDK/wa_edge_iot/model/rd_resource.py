# -*- coding: utf-8 -*-

# Copyright (C) 2017 Intel Corporation.  All rights reserved.
# Copyright (c) WanSheng Intelligent Corp. All rights reserved.

#
# Licensed under the Apache License, Version 2.0 (the "License");
#

from coapthon import defines

from .resource_data_base import ResourceDataBase
from ..framework.data_utility import DataMonitorParam
from builtins import str

class RdResource(object):
    
    def __init__(self, device):
        self.__device = device
        
        self.href = None
        self.resource_type = []
        self.attrs = None
        self.groups = []
        self.__property_config = {}
        self.__property_data_cache = None
    
    def equals(self, obj):
        return self.href == obj.href
    
    def get_absolute_uri(self):
        if self.get_owner_device() is None:
            return self.href
        
        if self.href.startswith("/"):
            return self.__device.get_absolute_uri() + self.href
        else:
            return self.__device.get_absolute_uri() + "/" + self.href



    def get_attrs(self):
        return self.attrs
    
    
    def get_groups(self):
        return self.groups
    
    def get_href(self):
        return self.href
    
    def get_owner_device(self):
        return self.__device
    
    
    def get_resource_type(self):
        return self.resource_type
    
    def hash_code(self):
        return hash(self.href)
    
    
    # monitor_name: 
    def enable_monitoring(self, 
               monitor_name = "",
               property_name = None,
               interval=10, 
               sequence=0, 
               process=False):
        
        assert type(interval) is int
        assert type(sequence) is int
        assert monitor_name is None or type(monitor_name) in [str, str]
        assert property_name is None or type(property_name) in [str, str]
        
        from ..framework.wagent import I_wagent

        ri = self.get_href()
        parameters = DataMonitorParam(
            self.get_owner_device().get_device_id(),
            ri,
            property_name,
            interval,
            sequence, 
            process , 
            monitor_name)
        
        return I_wagent().data_util().add_data_observer(parameters)
    
    def get(self, property_name = None):
        from ..framework.wagent import I_wagent

        data_util = I_wagent().data_util()
        
        is_property = True
        
        if property_name is None or property_name == "":
            uri = self.get_absolute_uri()
            is_property = False
        else :
            assert type(property_name) is str
            uri = self.get_absolute_uri() + "?pt=" + property_name
    
        response = data_util.get(uri)
        
        if response is None:
            return None
        
        if response.payload is None:
            return None
        
        if response.code == defines.Codes.CONTENT.number :
            fmt = response.content_type
            
            parser = data_util.get_parser(fmt)
            data = None
            if parser is None:
                data = ResourceDataBase(response.payload, fmt)
                
                # if reading property, directly add the property into resource data
                if is_property:
                    data.set_property_value(property_name, str(response.payload))
            else:
                data = parser.parse(response.payload, fmt)
            
            return data
        
        return None
    
    def put(self, format, payload, property_name = None):
        from ..framework.wagent import I_wagent
        data_util = I_wagent().data_util()
        url = self.get_absolute_uri()
        if property_name:
            url = url + '?pt=' + property_name
        
        return data_util.put(url, format, payload)
     
    def dump(self):
        print("uri:" + self.href)
                