# -*- coding: utf-8 -*-

# Copyright (C) 2017 Intel Corporation.  All rights reserved.
# Copyright (c) WanSheng Intelligent Corp. All rights reserved.

#
# Licensed under the Apache License, Version 2.0 (the "License");
#


class MediaTypeFormat(object):
    TEXT_PLAIN = 0
    TEXT_XML = 1
    TEXT_CSV = 2
    TEXT_HTML = 3
    IMAGR_GIF = 21
    IMAGE_JPEG = 22
    IMAGE_PNG = 23
    IMAGE_TIFF = 24
    AUDIO_RAW = 25
    VIDEO_RAW = 26
    APPLICATION_LINK_FORMAT = 40
    APPLICATION_XML = 41
    APPLICATION_OCTET_STREAM = 42
    APPLICATION_RDF_XML = 43
    APPLICATION_SOAP_XML = 44
    APPLICATION_ATOM_XML = 45
    APPLICATION_XMPP_XML = 46
    APPLICATION_EXI = 47
    APPLICATION_FASTINFOSET = 48
    APPLICATION_SOAP_FASTINFOSET = 49
    APPLICATION_JSON = 50
    APPLICATION_X_OBIX_BINARY = 51
    APPLICATION_JSON_OCF = 60
    APPLICATION_JSON_LWM2M = 88
