# -*- coding: utf-8 -*-

# Copyright (C) 2017 Intel Corporation.  All rights reserved.
# Copyright (c) WanSheng Intelligent Corp. All rights reserved.

#
# Licensed under the Apache License, Version 2.0 (the "License");
#

# from enum import Enum
import json

class StandardType(object):
    lwm2m = 1
    ocf = 2
    rest = 3
    publish = 4
    modbus = 5
    unknow = 6


class RdDevice(object):
    def __init__(self):
        self.device_id = ""
        self.standard_type = ""
        self.device_type = ""
        self.attrs = {}
        self.sleep_type = None
        self.ttl = None
        self.addr = None
        self.resources = []
        self.groups = []
        self.device_status = None

    def equals(self, obj):
        return self.device_id == obj.get_device_id()

    def get_absolute_uri(self):
        return "/dev" + "/" + self.device_id

    def get_addr(self):
        return self.addr

    def get_attrs(self):
        return self.attrs

    def get_device_id(self):
        return self.device_id

    def get_device_status(self):
        return self.device_status

    def get_device_type(self):
        return self.device_type

    def get_groups(self):
        return self.groups

    # def get_resource(self,uri):
    #    return self.resources
    # def get_resources(self):
    #    pass
    def get_resources(self):
            return self.resources

    def find_resource(self, uri):
        assert isinstance(uri, str)
        for resource in self.resources:
            if resource.get_href() == uri:
                return resource
        return None


    def get_sleep_type(self):
        return self.sleep_type

    def get_standard_type(self):
        return self.standard_type

    def get_ttl(self):
        return self.ttl

    def hash_code(self):
        return self.device_id.hash_code()

    def to_json(self):
        device_json = {}
        if self.device_id is not None:
            device_json["di"] = self.device_id
        
        if self.standard_type is not None:
            print("st =" + str(self.standard_type))
            device_json["st"] = self.standard_type
        
        if self.addr is not None:
            device_json["addr"] = self.addr
        if self.device_type is not None:
            device_json["dt"] = self.device_type
        if self.ttl is not None:
            device_json["ttl"] = self.ttl
        if self.sleep_type is not None:
            device_json["set"] = self.sleep_type
        if self.device_status is not None:
            device_json["status"] = self.device_status
        if self.groups is not None: 
            device_json["groups"] = self.groups
        if self.attrs is not None:
            device_json["attrs"] = self.attrs
        
        resource_array = []
        if self.resources is not None and len(self.resources) != 0:
            for resource in self.resources:
                obj = {}
                if resource.href is not None:
                    obj["href"] = resource.href 
                if resource.resource_type is not None:
                    obj["rt"] = resource.resource_type 
                if resource.groups is not None:
                    obj["groups"] = resource.groups
                if resource.attrs is not None:
                    obj["attrs"] = resource.attrs 
                resource_array.append(obj)
               
        device_json["links"] = resource_array
        # print "device_json links=", device_json["links"]
        return json.dumps(device_json)

    def dump(self, dump_resource = False):
        print("device info:")
        print("\tdi:" + self.device_id)
        print("\tstatus:" + self.device_status)
        print("\tresources:" + str(len(self.resources)))
        
        if (dump_resource):
            print("resources:")
            for  res in self.resources:
                res.dump()
            print("resource end..")
