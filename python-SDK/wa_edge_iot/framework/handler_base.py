# Copyright (c) WanSheng Intelligent Corp. All rights reserved.


from .data_process import  DataProcess
from ..model.rd_device import RdDevice
from ..model.resource_data_base import ResourceDataBase

class CacheDbHandlersBase(object):
    '''
    classdocs
    '''
    
    Sys_Event_Cloud_Connection = 1
    Sys_Event_CacheDB_Connection = 2
    

    def __init__(self):
        '''
        Constructor
        '''
    # Available only when redis is used and connected
    def on_system_event(self, event_type, event_msg):
        
        return

    # callback for any device registration and de-registration
    # Always available when redis is used and connected
    
    def on_any_device_registration(self, di, status):    
       
        return
    
    # callback for cached data in redis has been changed for monitored resources
    # Always available when redis is used and connected
    
    def on_cached_data_updated(self, di, ri, property, info):
        
        return

    def on_data_tag(self, di, ri, property, tag):
        
        return

    # AMS config file update event
    def on_ams_cfg_event(self, product_name, target_type, target_id, config_path):
        
        return    
    
class CoAPHandlersBase(object):
    def __init__(self):
        '''
        Constructor
        '''

    # Handle device change of monitored devices. 
    # It is available after calling I_wagent().add_device_monitor().
    def on_monitored_device_change(self, devices, monitor_name):
        #assert isinstance(device, RdDevice)
        return
    
    # 
    # The data event callback for I_wagent().add_data_monitor_point(),
    # or RdResource().enable_monitoring().
    # It is available for any of following situation:
    # 1.  the parameter "monitor_name" is not None 
    # 2.  the parameter "process" is "True"
    # 3. redis is not configured
    # If process parameter is False, the data_process passed in is None.
    def on_resource_data_process(self, di, ri, data, monitor_name, data_process):
        assert data_process is None or isinstance(data_process, DataProcess)
        assert isinstance(data, ResourceDataBase)
        return
    