# -*- coding: utf-8 -*-

# Copyright (C) 2017 Intel Corporation.  All rights reserved.
# Copyright (c) WanSheng Intelligent Corp. All rights reserved.

# Licensed under the Apache License, Version 2.0 (the "License");



class DataProcess():
    CONTINUE = 0
    DROP = 1
    MODIFY = 2
    def __init__(self, default_fmt = 0):
        self.__data = None
        self.__decision = self.CONTINUE
        self.__modified_data = None
        self.__data_fmt = default_fmt
    
    def modify_data(self, payload, format = None):
        self.__decision = self.MODIFY
        self.__modified_data = payload
        if format:
            self.__data_fmt = format
        
    def set_decision(self, decision):
        self.__decision = decision
        
    def get_decision(self):
        return self.__decision

    def get_modified_data(self):
        return (self.__modified_data, self.__data_fmt )