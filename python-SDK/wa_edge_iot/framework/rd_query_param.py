# Copyright (c) WanSheng Intelligent Corp. All rights reserved.

import json



def find_query_key(query, key):
    for item in query.split('&'):
        k = item.split("=")
        if len(k) == 2:
            if k[0] == key:
                return k[1]
    return None



class RDQueryParam(object):
    def __init__(self, 
                 device_id = None,
                 standard_type = None,
                 device_type = None,
                 resource_type = None,
                 groups = None,
                 resource_types = None
                 ):
        self.query = {}
        self.group_ids = None
        self.with_rts = None
        
        if device_id is not None:
            self.query['di'] = device_id
        if standard_type is not None:
            self.query['st'] = standard_type
        if device_type is not None:
            self.query['dt'] = device_type
        if resource_type is not None:
            self.query['rt'] = resource_type
            
        if type(groups) is  list:
            self.group_ids = groups
            self.query['groups'] = groups
        elif type(groups) in [str, str]:
            self.group_ids[0] = groups
            self.query['groups'] = self.group_ids
                    
        if type(resource_types) is list:
            self.with_rts = resource_types
            self.query['with_rts'] = resource_types
        elif type(resource_types) in [str, str]:
            self.group_ids[0] = resource_types
            self.query['with_rts'] = self.with_rts


    # compose a URL query string:
    # di={}&st={}&dt={}&rt={}&with_rts=[rt1,rt2]&groups=[group1, group2]
    def to_query_string(self):
        bf = []
        if 'di' in self.query:
            bf.append("&di=" + self.query['di'])
        if 'st' in self.query:
            bf.append("&st=" + self.query['st'])
        if 'dt' in self.query:
            bf.append("&dt=" + self.query['dt'])
        if 'rt' in self.query:
            bf.append("&rt=" + self.query['rt'])
        if self.group_ids:
            bf.append("&groups=")
            ids = []
            ids.append("[")
            l = len(self.group_ids)
            for i in range(l):
                if i == (l - 1):
                    ids.append(self.group_ids[i] + "]")
                else:
                    ids.append(self.group_ids[i] + ",")
            bf.append("".join(ids))
        if self.with_rts:
            bf.append("&with_rts=")
            ids = []
            l = len(self.with_rts)
            for i in range(l):
                if i == (l - 1):
                    ids.append(self.with_rts[i] + "]")
                else:
                    ids.append(self.with_rts[i] + ",")
            bf.append("".join(ids))
        bf = ''.join(bf)
        s =  bf.replace("&", "?", 1) if len(bf) > 0 else bf
        
        return s

    def to_monitor_string(self, publish_addr):
        self.query["pub-addr"] = publish_addr
        self.query["pub-type"] = "coap" 
        s = json.dumps(self.query)
        
        return s