# -*- coding: utf-8 -*-

# Copyright (C) 2017 Intel Corporation.  All rights reserved.
# Copyright (c) WanSheng Intelligent Corp. All rights reserved.

#
# Licensed under the Apache License, Version 2.0 (the "License");
#


import json
from wa_edge_iot.model.rd_resource import RdResource
from wa_edge_iot.model.rd_device import RdDevice
from wa_edge_iot.model.rd_device import StandardType

import traceback


def _parse_resource(json_text, device):
    resource_list = []
    json_array = json_text
    for resource_json_object in json_array:
        href = resource_json_object.get("href")
        attrs = resource_json_object.get("attrs")
        resource_type = resource_json_object.get("rt")
        groups = resource_json_object.get("groups")
        if "inst" in resource_json_object:
            for inst in resource_json_object["inst"]:
                resource = RdResource(device)
                resource.href = href + "/" + inst
                resource.attrs = attrs
                resource.resource_type = resource_type
                resource.groups = groups
                resource_list.append(resource)
        else:
            resource = RdResource(device)
            resource.href = href
            resource.attrs = attrs
            resource.resource_type = resource_type
            resource.groups = groups
            resource_list.append(resource)

    return resource_list



class RDParser(object):

    @staticmethod
    def make_device(device_json_object):
                device = RdDevice()
                device.device_id = device_json_object["di"]
                device.device_status = device_json_object["status"]
                device.standard_type = device_json_object["st"]
                device.addr = device_json_object[
                    "addr"] if 'addr' in device_json_object else None
                device.groups = device_json_object[
                    "groups"] if 'groups' in device_json_object else []
                device.attrs = device_json_object[
                    "attrs"] if 'attrs' in device_json_object else {}
                device.device_type = device_json_object["dt"]
                device.sleep_type = device_json_object["set"]

                resource_list = _parse_resource(device_json_object["links"], device)
                device.resources = resource_list
                
                return device

    
    @staticmethod
    def parse_device(data_in_json):
        if data_in_json is None or "" == data_in_json:
            return None
        try:
            device_list = []
            data_in_json = data_in_json[ :-1] if data_in_json[-1] != ']' and  data_in_json[-1] != '}' else data_in_json  # delete strange last char
            json_array = json.loads(data_in_json)
            if type(json_array) is dict:
                device = RDParser.make_device(json_array)
                device_list.append(device)
            elif type(json_array) is list:
                for device_json_object in json_array:
                    device = RDParser.make_device(device_json_object)
                    device_list.append(device)
                    
            return device_list

        except Exception as e:
            print("" + str(e))
            return None
    
