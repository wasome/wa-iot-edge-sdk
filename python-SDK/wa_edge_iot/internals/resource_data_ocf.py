# -*- coding: utf-8 -*-

# Copyright (C) 2017 Intel Corporation.  All rights reserved.
# Copyright (c) WanSheng Intelligent Corp. All rights reserved.

#
# Licensed under the Apache License, Version 2.0 (the "License");
#


# import sys
# sys.path.append("../utilities")
from ..model.constants import MediaTypeFormat
from ..model.resource_data_base import ResourceDataBase
import json


class ResourceDataOCF(ResourceDataBase):
    def __init__(self, rawpayload, fmt):
        super(ResourceDataOCF, self).__init__(rawpayload, fmt)
        self.prm = None

    def get_format(self):
        return MediaTypeFormat.APPLICATION_JSON
    
    def pack(self):
        obj = {}
        if self.prm is not None:
            obj["prm"] = self.prm
            
        for data in self.items:
            obj[data.prop_name] = data.value
        return MediaTypeFormat.APPLICATION_JSON, json.dumps(obj)
