
IF (NOT DEFINED WA_SDK_DIR)
set(WA_SDK_DIR ${CMAKE_CURRENT_LIST_DIR})
message ("wa-sdk.cmake: WA_SDK_DIR is not defined. Set to default: ${WA_SDK_DIR}")
ENDIF()

include_directories(${WA_SDK_DIR}/include)

IF (NOT WA_CMAKE_NOT_INCLUDE_SUB_DIRS)
include_directories(${WA_SDK_DIR}/include/coap)
include_directories(${WA_SDK_DIR}/include/wa-sdk)
include_directories(${WA_SDK_DIR}/include/wa-core)
include_directories(${WA_SDK_DIR}/include/azureiot)
include_directories(${WA_SDK_DIR}/include/umock_c)
include_directories(${WA_SDK_DIR}/include/hiredis)

include_directories(${WA_SDK_DIR}/include/wa_iot_gateway_sdk-1.0.0)
include_directories(${WA_SDK_DIR}/include/nanomsg)
include_directories(${WA_SDK_DIR}/include/azure_macro_utils)
include_directories(${WA_SDK_DIR}/include/mbedtls)
ENDIF()

link_directories(${WA_SDK_DIR}/lib)


#
# WA_SDK_LINK_DEPS: used for target_link_libraries() 
#
set(WA_SDK_LINK_DEPS wa_iot hiredis gateway aziotsharedutil_dll nanomsg pthread dl rt)

function(linkWA_SDK whatIsBuilding)
  target_link_libraries(${whatIsBuilding} ${WA_SDK_LINK_DEPS} m)
endfunction(linkWA_SDK)


set(WA_SDK_STATIC_LINK_DEPS wa_iot_static gateway_static aziotsharedutil pthread dl rt)
function(link_static_WA_SDK whatIsBuilding)
  target_link_libraries(${whatIsBuilding} ${WA_SDK_STATIC_LINK_DEPS} stdc++ m)
endfunction(link_static_WA_SDK)