#!/bin/sh

# README:
# =======
# the cross compiler tools are available: 
# set ARM_A7_STAGING_DIR and ARM_A7_SDKTARGETSYSROOT in your .bashrc for the sysroot path in your dev machine



# sudo apt-get install g++-aarch64-linux-gnu
# sudo apt-get install gcc-aarch64-linux-gnu

if [ ! -n "$ARM64_STAGING_DIR" ]; then
# export STAGING_DIR="/home/beihai/cross-toolchains/toolchain-arm64_gcc-imx8mm"
    if [ ! -f /usr/bin/aarch64-linux-gnu-gcc ]; then
        echo "installing gcc..."
        sudo apt-get install gcc-aarch64-linux-gnu
    fi
    if [ ! -f /usr/bin/aarch64-linux-gnu-g++ ]; then
        echo "installing g++..."
        sudo apt-get install g++-aarch64-linux-gnu
    fi

    export TOOLCHAIN_DIR="/usr"

else
export TOOLCHAIN_DIR="$ARM64_STAGING_DIR"
fi

export CC=${TOOLCHAIN_DIR}/bin/aarch64-linux-gnu-gcc
export CXX=${TOOLCHAIN_DIR}/bin/aarch64-linux-gnu-g++
export CONFIGURE_HOST="--host=aarch64-linux-gnu"

if [ ! -n "$ARMA64_SDKTARGETSYSROOT" ]; then
    # export SDKTARGETSYSROOT="/home/beihai/cross-toolchains/toolchain-arm64_gcc-imx8mm/aarch64-linux-gnu/libc"
    echo "ARMA64_SDKTARGETSYSROOT is not defined."
else
export SDKTARGETSYSROOT="$ARMA64_SDKTARGETSYSROOT"
fi

echo "set env for ARM A64 build"
echo "SDKTARGETSYSROOT_ROOT="$SDKTARGETSYSROOT
echo "TOOLCHAIN_DIR="$TOOLCHAIN_DIR
