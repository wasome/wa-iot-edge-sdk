INCLUDE(CMakeForceCompiler)

SET(CMAKE_SYSTEM_NAME Linux) # this one is important
SET(CMAKE_SYSTEM_VERSION 1) # this one not so much

message(STATUS "*** ARM64  toolchain file ***")
set(CMAKE_VERBOSE_MAKEFILE ON) 
SET(CMAKE_SYSROOT $ENV{SDKTARGETSYSROOT})
SET(SDKTARGETSYSROOT $ENV{SDKTARGETSYSROOT})
SET (toolchain_sdk_dir $ENV{TOOLCHAIN_DIR})
IF ((UNDEFINED toolchain_sdk_dir) OR ("${toolchain_sdk_dir}" STREQUAL ""))
    SET (toolchain_sdk_dir "/usr")
    message(STATUS "set toolchain_sdk_dir to /usr")
ENDIF()

message(STATUS "SDKTARGETSYSROOT=${SDKTARGETSYSROOT}")
message(STATUS "toolchain_sdk_dir=${toolchain_sdk_dir}")


SET(CMAKE_C_COMPILER ${toolchain_sdk_dir}/bin/aarch64-linux-gnu-gcc)
SET(CMAKE_CXX_COMPILER ${toolchain_sdk_dir}/bin/aarch64-linux-gnu-g++)

 

# this is the file system root of the target
SET(CMAKE_FIND_ROOT_PATH ${SDKTARGETSYSROOT})

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

