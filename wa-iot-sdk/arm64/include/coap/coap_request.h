/*
 * Copyright (C) 2017 Intel Corporation.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */


// Copyright (c) WanSheng Intelligent Corp. All rights reserved.


#ifndef SRC_COAP_REQUEST_H_
#define SRC_COAP_REQUEST_H_

#include "coap_platforms.h"
#include "er-coap.h"
#include "er-coap-transactions.h"
//#include "rest-engine.h"

#ifdef __cplusplus
extern "C" {
#endif




// return
// STOP_REQUEST : stop the whole request procedure
#define STOP_REQUEST -1




typedef coap_packet_t rest_request_t;
typedef coap_packet_t rest_response_t;

#define COAP_CONTEXT_NONE  NULL

void coap_init_engine(void);
coap_context_t * coap_context_new(uip_ipaddr_t *my_addr);
void coap_context_init(coap_context_t * context);

/*---------------------------------------------------------------------------*/
/*- Client Part -------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void coap_nonblocking_put(
				coap_context_t *coap_ctx,
				uip_ipaddr_t *dst_addr,
				coap_packet_t *request,
				restful_response_handle_f request_callback,
				void * user_data);

void coap_nonblocking_get(
				coap_context_t *coap_ctx,
				uip_ipaddr_t *dst_addr,
				coap_packet_t *request,
				restful_response_handle_f request_callback,
				void * user_data);

// this API cover both get, put and post
void coap_nonblocking_request(
				coap_context_t *coap_ctx,
				uip_ipaddr_t *dst_addr,
				coap_packet_t *request,
				restful_response_handle_f request_callback,
				void * user_data);

void coap_get_async( coap_context_t *coap_ctx,
                const char *url, const char * query,
                uip_ipaddr_t *dst_addr,
                restful_response_handle_f request_callback,
                void * user_data);

void coap_get_async( coap_context_t *coap_ctx,
                const char *url, const char * query,
                uip_ipaddr_t *dst_addr,
                restful_response_handle_f request_callback,
                void * user_data);
void coap_put_async( coap_context_t *coap_ctx,
                const char *url, const char * query, int format,
                void* payload, int payload_len,
                uip_ipaddr_t *dst_addr,
                restful_response_handle_f request_callback,
                void * user_data);
void coap_post_aync( coap_context_t *coap_ctx,
                const char *url, const char * query, int format,
                void* payload, int payload_len,
                uip_ipaddr_t *dst_addr,
                restful_response_handle_f request_callback,
                void * user_data);

int
coap_handle_packet(coap_context_t *coap_ctx);

uint8_t coap_is_request(coap_packet_t * coap_message);




#ifdef __cplusplus
}
#endif

#endif /* SRC_COAP_REQUEST_H_ */
