
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#ifndef APPS_GW_BROKER_PLUGIN_SDK_INCLUDE_PLUGIN_CONSTANTS_H_
#define APPS_GW_BROKER_PLUGIN_SDK_INCLUDE_PLUGIN_CONSTANTS_H_

#ifdef __cplusplus
extern "C"
{
#endif



typedef enum
{
    T_Default = 0,
    T_Get = 1,
    T_Post,
    T_Put,
    T_Del,
    T_Observe,
    T_Obs_Attr,
    MAX_RESTFUL_ACTION
} rest_action_t;



typedef enum
{
    IA_TEXT_PLAIN = 0,
    IA_LWM2M_CONTENT_TEXT = 0,
    IA_LWM2M_CONTENT_LINK = 40,
    IA_LWM2M_CONTENT_OPAQUE = 42,
    IA_LWM2M_CONTENT_TLV = 89, //1542,
    IA_LWM2M_CONTENT_JSON = 88, //1543,
    IA_LWM2M_CONTENT_JSON_ORIG = 1543,
    IA_APPLICATION_JSON = 50,
    IA_APPLICATION_CBOR = 60,
    IA_OCF_CBOR = 60
}wa_format_type_t;




/// XK_TAG
#define TAG_REST_REQ "req"
#define TAG_REST_RESP "resp"
#define TAG_EVENT "evt"


/// XK_ACTION
#define ACTION_GET "GET"
#define ACTION_PUT "PUT"
#define ACTION_POST "POST"
#define ACTION_DEL "DEL"
#define ACTION_OBS "OBSERVE"
#define ACTION_ATTR "ATTR"

#define ACTION_CREATE "CREATE"


#define XK_TAG "_tag"

#define XK_MID "_id"
#define XK_URI "_uri"
#define XK_QUERY "_qry"
#define XK_PAYLOAD "_pa"
#define XK_ACTION "_ac"
#define XK_FMT "_fmt"
#define XK_TAG "_tag"
#define XK_DEST "_dest"
#define XK_SRC "_src"
#define XK_OBS "_obs"
#define XK_PUBLISH "_pub"
#define XK_RESP_CODE "_status"
#define XK_TM   "_tm"




#ifdef __cplusplus
}
#endif

#endif /* APPS_GW_BROKER_PLUGIN_SDK_INCLUDE_PLUGIN_CONSTANTS_H_ */
