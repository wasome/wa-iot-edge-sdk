/*
 * Copyright (C) 2017 Intel Corporation.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

// Copyright (c) WanSheng Intelligent Corp. All rights reserved.

#ifndef __ams_path_h___
#define __ams_path_h___

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define PRODUCT_KEY "product"

#define PATH_LEN 256
char * ams_path_internal_buf();
bool ams_set_product_name(char * name);
const char * get_product_name();
bool ams_set_product_root(char * root_dir);
bool ams_locate_product_path(char * execpath, char * bin_path);
char * ams_my_product_root(char * buf, int buf_len);
char * ams_get_product_scripts_dir(char * path);
char * ams_make_temp_dir(char * dir, char * subdir);
char * ams_get_temp_root(char * dir, int buf_len);
char * ams_get_temp_pathname(char * pathname, const char * prefix);
const char* ams_get_running_root();


char * get_sw_product_root(const char * product, char * path);
char * get_sw_product_dir(const char * product, char * path);
char * get_sw_product_config_dir(const char * product, char * path);
char * get_sw_product_scripts_dir(const char * product, char * path);
char * get_sw_product_config_pathname(const char * product_name, char *target_type, char * target_id, char * out_path);

void wa_config_set_target(const char* target_type, const char* target_id);


#ifdef __cplusplus
}
#endif


#endif