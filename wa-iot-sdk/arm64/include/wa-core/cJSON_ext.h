
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#ifndef __cjson_ext_h__
#define __cjson_ext_h__
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "cJSON.h"

#ifdef __cplusplus
extern "C" {
#endif


cJSON * load_json_file_A(const char* path);
bool save_json_file(cJSON * json, const char* path);
cJSON * cJSON_Parse_Data(const char *value, int len);
const char *cJSONx_GetObjectString(cJSON * object, const char* item);
int cJSONx_GetObjectNumber(cJSON * object, const char* item, int default_val);



#ifdef __cplusplus
}
#endif


#endif