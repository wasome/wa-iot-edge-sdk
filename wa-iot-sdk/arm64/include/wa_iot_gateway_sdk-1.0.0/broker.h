// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

/** @file        broker.h
*    @brief        Library for configuring and using the gateway's message broker.
*
*    @details    This is the API to create a reference counted and thread safe 
*                gateway message broker. The broker sends messages to other
*                modules. Messages published to the broker have a bag of
*               properties (name, value) and an opaque array of bytes that
*               constitute the message content.
*/

#ifndef BROKER_H
#define BROKER_H

/** @brief Struct representing a message broker. */
typedef struct BROKER_HANDLE_DATA_TAG* BROKER_HANDLE;

#include "azure_c_shared_utility/macro_utils.h"
#include "message.h"
#include "module.h"
#include "gateway_export.h"

#ifdef __cplusplus
#include <cstddef>
extern "C"
{
#else
#include <stddef.h>
#endif

/** @brief    Link Data with #MODULE_HANDLE for source and sink. 
*/
typedef struct BROKER_LINK_DATA_TAG {
    /** @brief    #MODULE_HANDLE representing the module generating/publishing messages. 
    */
    MODULE_HANDLE module_source_handle;
    /** @brief    #MODULE_HANDLE representing the module receiving messages. 
    */
    MODULE_HANDLE module_sink_handle;
} BROKER_LINK_DATA;

#define BROKER_RESULT_VALUES \
    BROKER_OK, \
    BROKER_ERROR, \
    BROKER_ADD_LINK_ERROR, \
    BROKER_REMOVE_LINK_ERROR, \
    BROKER_INVALIDARG

/** @brief    Enumeration describing the result of ::Broker_Publish, 
*            ::Broker_AddModule, ::Broker_AddLink, and ::Broker_RemoveModule.
*/
MU_DEFINE_ENUM_WITHOUT_INVALID(BROKER_RESULT, BROKER_RESULT_VALUES);

/** @brief        Creates a new message broker.
*   
*    @return        A valid #BROKER_HANDLE upon success, or @c NULL upon failure.
*/
GATEWAY_EXPORT BROKER_HANDLE Broker_Create(void);

/** @brief        Increments the reference count of a message broker.
*
*    @details    This function will simply increment the internal reference
*                count of the provided #BROKER_HANDLE.
*
*    @param        broker  The #BROKER_HANDLE to be cloned.
*/

GATEWAY_EXPORT BROKER_HANDLE Broker_Create2(void * gateway_handle);


GATEWAY_EXPORT void Broker_IncRef(BROKER_HANDLE broker);

/** @brief        Decrements the reference count of a message broker.
*
*    @details    This function will simply decrement the internal reference
*                count of the provided #BROKER_HANDLE, destroying the message
*                broker when the reference count reaches 0.
*
*    @param        broker  The #BROKER_HANDLE whose ref count will be decremented.
*/
GATEWAY_EXPORT void Broker_DecRef(BROKER_HANDLE broker);

/** @brief        Publishes a message to the message broker.
*
*    @details    For details about threading with regard to the message broker
*                and modules connected to it, see
*                <a href="https://github.com/Azure/azure-iot-gateway-sdk/blob/master/core/devdoc/broker_hld.md">Broker High Level Design Documentation</a>.
*
*    @param        broker    The #BROKER_HANDLE onto which the message will be
*                        published.
*    @param        source    The #MODULE_HANDLE from which the message will be
*                        published. The broker will not publish the message to this
*                       module. (optional, may be NULL)
*    @param        message    The #MESSAGE_HANDLE representing the message to be
*                        published.
*
*    @return        A #BROKER_RESULT describing the result of the function.
*/
GATEWAY_EXPORT BROKER_RESULT Broker_Publish(BROKER_HANDLE broker, MODULE_HANDLE source, MESSAGE_HANDLE message);

/** @brief        Adds a module to the message broker.
*
*    @details    For details about threading with regard to the message broker
*                and modules connected to it, see 
*                <a href="https://github.com/Azure/azure-iot-gateway-sdk/blob/master/core/devdoc/broker_hld.md">Broker High Level Design Documentation</a>.
*
*    @param        broker          The #BROKER_HANDLE onto which the module will be 
*                                added.
*    @param        module            The #MODULE for the module that will be added 
*                                to this message broker.
*
*    @return        A #BROKER_RESULT describing the result of the function.
*/
GATEWAY_EXPORT BROKER_RESULT Broker_AddModule(BROKER_HANDLE broker, const MODULE* module, const char * module_name);

/** @brief        Removes a module from the message broker.
*   
*    @param        broker    The #BROKER_HANDLE from which the module will be removed.
*    @param        module    The #MODULE of the module to be removed.
*   
*    @return        A #BROKER_RESULT describing the result of the function.
*/
GATEWAY_EXPORT BROKER_RESULT Broker_RemoveModule(BROKER_HANDLE broker, const MODULE* module);

/** @brief        Adds a route to the message broker.
*
*    @details    For details about threading with regard to the message broker
*                and modules connected to it, see
*                <a href="https://github.com/Azure/azure-iot-gateway-sdk/blob/master/core/devdoc/broker_hld.md">Broker High Level Design Documentation</a>.
*
*    @param        broker          The #BROKER_HANDLE onto which the module will be
*                                added.
*    @param        link            The #BROKER_LINK_DATA for the link that will be added
*                                to this message broker.
*
*    @return        A #BROKER_RESULT describing the result of the function.
*/
GATEWAY_EXPORT BROKER_RESULT Broker_AddLink(BROKER_HANDLE broker, const BROKER_LINK_DATA* link);

/** @brief        Removes a route from the message broker.
*
*    @param        broker    The #BROKER_HANDLE from which the link will be removed.
*    @param        link    The #BROKER_LINK_DATA of the link to be removed.
*
*    @return        A #BROKER_RESULT describing the result of the function.
*/
GATEWAY_EXPORT BROKER_RESULT Broker_RemoveLink(BROKER_HANDLE broker, const BROKER_LINK_DATA* link);

/** @brief      Disposes of resources allocated by a message broker.
*
*    @param      broker  The #BROKER_HANDLE to be destroyed.
*/
GATEWAY_EXPORT void Broker_Destroy(BROKER_HANDLE broker);


enum
{
    NN_Mod_Src,
    NN_Mod_Exit,
    NN_Mod_Dest,
    NN_Mod_Request
};

#define NN_TOPIC_SET_TYPE(buf, nn_type)     (*((uint16_t*)buf) = nn_type)
#define NN_TOPIC_GET_TYPE(buf)              (*((uint16_t*)buf))
#define NN_TOPIC_BODY_ADDR(buf)             ((char*)buf + NN_TYPE_SIZE)

// use two bytes for the nanomsg pub/sub type 
#define NN_TYPE_SIZE       2
#define NN_MID_SIZE        4
#define NN_MOD_ID_SIZE     sizeof(MODULE_HANDLE)

#define NN_RESP_LEADING_SIZE (NN_TYPE_SIZE + NN_MOD_ID_SIZE + NN_MID_SIZE)
#define NN_REQ_LEADING_SIZE(uri_len) (NN_TYPE_SIZE + NN_MOD_ID_SIZE + NN_MID_SIZE + uri_len)


// return milliseconds to the next check. -1: No check
typedef int (*Restful_Framework_Handler) (void * restful_context,  
    MODULE_HANDLE moduleHandle, const unsigned char * message, int message_len);


GATEWAY_EXPORT BROKER_RESULT ExBroker_SetRestfulFrameworkInternal(BROKER_HANDLE broker, 
    MODULE_HANDLE module, 
    Restful_Framework_Handler handler, 
    void * restful_context);


GATEWAY_EXPORT BROKER_RESULT ExBroker_SendResponseInternal(BROKER_HANDLE broker, 
    MODULE_HANDLE dest, MESSAGE_HANDLE message, uint32_t mid);


// Parameter command - definitions
#define MOD_CMD_REST_ENABLED    1
#define MOD_CMD_REST_WAKEUP     2
GATEWAY_EXPORT BROKER_RESULT ExBroker_SendModuleCmdInternal(BROKER_HANDLE broker, 
    MODULE_HANDLE module, uint32_t command);


GATEWAY_EXPORT BROKER_RESULT ExBroker_SendModuleCmdInternal(BROKER_HANDLE broker, 
    MODULE_HANDLE dest, uint32_t command);

GATEWAY_EXPORT BROKER_RESULT ExBroker_SendRequestInternal(BROKER_HANDLE broker, MODULE_HANDLE source, 
    MESSAGE_HANDLE message, const char * topic, uint32_t mid);

GATEWAY_EXPORT BROKER_RESULT ExBroker_RegisterNNTopicInternal(BROKER_HANDLE broker, MODULE_HANDLE module, const char * topic_uri);

/*
    @return the gateway handle given during creating the broker.
            note: it may be NULL. 
*/
GATEWAY_EXPORT void * ExBroker_Gateway_Handle(BROKER_HANDLE broker);


/*
    @return module name loaded from the JSON configuration file.
            note: it may be NULL. 
*/
const char * ExBroker_Module_Name(BROKER_HANDLE broker, MODULE_HANDLE module);


#ifdef __cplusplus
}
#endif


#endif /*BROKER_H*/
