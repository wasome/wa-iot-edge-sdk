#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "aziotsharedutil" for configuration "Release"
set_property(TARGET aziotsharedutil APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(aziotsharedutil PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "C"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libaziotsharedutil.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS aziotsharedutil )
list(APPEND _IMPORT_CHECK_FILES_FOR_aziotsharedutil "${_IMPORT_PREFIX}/lib/libaziotsharedutil.a" )

# Import target "aziotsharedutil_dll" for configuration "Release"
set_property(TARGET aziotsharedutil_dll APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(aziotsharedutil_dll PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libaziotsharedutil_dll.so"
  IMPORTED_SONAME_RELEASE "libaziotsharedutil_dll.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS aziotsharedutil_dll )
list(APPEND _IMPORT_CHECK_FILES_FOR_aziotsharedutil_dll "${_IMPORT_PREFIX}/lib/libaziotsharedutil_dll.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
