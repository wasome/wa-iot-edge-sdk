#!/bin/sh

# README:
# =======
# the cross compiler tools are available: 
# set ARM_A7_STAGING_DIR and ARM_A7_SDKTARGETSYSROOT in your .bashrc for the sysroot path in your dev machine


# sudo apt-get install g++-arm-linux-gnueabihf
# sudo apt-get install gcc-arm-linux-gnueabihf


if [ ! -n "$ARM_A7_STAGING_DIR" ]; then
    echo "ARM_A7_STAGING_DIR not defined"
    if [ ! -f /usr/bin/arm-linux-gnueabihf-gcc ]; then
        echo "installing gcc..."
        sudo apt-get install gcc-arm-linux-gnueabihf
    fi
    if [ ! -f /usr/bin/arm-linux-gnueabihf-g++ ]; then
        echo "installing g++..."
        sudo apt-get install gcc-arm-linux-gnueabihf
    fi

    export TOOLCHAIN_DIR="/usr"

else
    export TOOLCHAIN_DIR="$ARM_A7_STAGING_DIR"
fi

[ -n "$WA_C_COMPILER" ] || export WA_C_COMPILER="arm-linux-gnueabihf-gcc"
[ -n "$WA_CXX_COMPILER" ] || export WA_CXX_COMPILER="arm-linux-gnueabihf-g++"

export CC=${TOOLCHAIN_DIR}/bin/${WA_C_COMPILER}
export CXX=${TOOLCHAIN_DIR}/bin/${WA_CXX_COMPILER}
export CONFIGURE_HOST="--host=arm-linux-gnueabihf"


if [ ! -n "$ARM_A7_SDKTARGETSYSROOT" ]; then
    if [ -d $TOOLCHAIN_DIR/arm-linux-gnueabihf/libc ]; then
        export SDKTARGETSYSROOT="$TOOLCHAIN_DIR/arm-linux-gnueabihf/libc"
    else
        echo "SDKTARGETSYSROOT not defined. Use default"
        # export SDKTARGETSYSROOT="/toolchains/maichong/gcc-linaro-arm-linux-gnueabihf-4.7-2013.03-20130313_linux/arm-linux-gnueabihf/libc"
    fi
else
    export SDKTARGETSYSROOT="$ARM_A7_SDKTARGETSYSROOT"
fi

echo "set env for ARM A7 build"
echo "SDKTARGETSYSROOT_ROOT="$SDKTARGETSYSROOT
echo "TOOLCHAIN_DIR="$TOOLCHAIN_DIR
echo "WA_C_COMPILER="$WA_C_COMPILER
echo "WA_CXX_COMPILER="$WA_CXX_COMPILER

