INCLUDE(CMakeForceCompiler)

SET(CMAKE_SYSTEM_NAME Linux) # this one is important
SET(CMAKE_SYSTEM_VERSION 1) # this one not so much

message(STATUS "*** ARM A7 toolchain file ***")
set(CMAKE_VERBOSE_MAKEFILE ON) 
SET(CMAKE_SYSROOT $ENV{SDKTARGETSYSROOT})
SET(SDKTARGETSYSROOT $ENV{SDKTARGETSYSROOT})
SET (toolchain_sdk_dir $ENV{TOOLCHAIN_DIR})
IF ((UNDEFINED toolchain_sdk_dir) OR ("${toolchain_sdk_dir}" STREQUAL ""))
    SET (toolchain_sdk_dir "/usr")
    message(STATUS "set toolchain_sdk_dir to /usr")
ENDIF()


message(STATUS "SDKTARGETSYSROOT=${SDKTARGETSYSROOT}")
message(STATUS "toolchain_sdk_dir=${toolchain_sdk_dir}")
message(STATUS "WA_C_COMPILER=$ENV{WA_C_COMPILER}")
message(STATUS "WA_CXX_COMPILER=$ENV{WA_CXX_COMPILER}")

# the gcc arm v4.6 don't support --add_needed


IF ("$ENV{WA_C_COMPILER}"  STREQUAL "")
    SET(CMAKE_C_COMPILER ${toolchain_sdk_dir}/bin/arm-linux-gnueabihf-gcc)
ELSE()
    SET(CMAKE_C_COMPILER "${toolchain_sdk_dir}/bin/$ENV{WA_C_COMPILER}")
ENDIF()
message(STATUS "CMAKE_C_COMPILER=${CMAKE_C_COMPILER}")

IF ("$ENV{WA_CXX_COMPILER}" STREQUAL "")
SET(CMAKE_CXX_COMPILER ${toolchain_sdk_dir}/bin/arm-linux-gnueabihf-g++)    
ELSE()
SET(CMAKE_CXX_COMPILER ${toolchain_sdk_dir}/bin/$ENV{WA_CXX_COMPILER})
ENDIF()
message(STATUS "CMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}")


# this is the file system root of the target
IF ( NOT "${SDKTARGETSYSROOT}" STREQUAL "")
SET(CMAKE_FIND_ROOT_PATH ${SDKTARGETSYSROOT})
ENDIF()

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

