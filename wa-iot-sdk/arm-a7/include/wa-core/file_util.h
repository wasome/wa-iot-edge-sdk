
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#ifndef PLUGIN_SDK_SRC_FILE_UTIL_H_
#define PLUGIN_SDK_SRC_FILE_UTIL_H_

#include "md5.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


#ifdef __cplusplus
extern "C" {
#endif

#define MD5_HASH_SIZE (MD5_DIGEST_LENGTH*2 + 2)

/*
    @param  hash:  the size must be bigger than MD5_HASH_SIZE
*/
bool get_file_md5(char * filepath, char * hash);
int wa_extract_package_tar_gz(const char * package_file, const char * target_dir);
bool wa_copy_file(const char * src_path, const char * dest_path, bool replace);


#ifdef __cplusplus
}
#endif


#endif /* PLUGIN_SDK_SRC_FILE_UTIL_H_ */
