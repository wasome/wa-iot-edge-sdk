/*
 * Copyright (C) 2017 Intel Corporation.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIB_CRYPTO_H_
#define LIB_CRYPTO_H_




#ifdef __cplusplus
extern "C" {
#endif


/// crypto_utils.c
unsigned char * i_md5(const char* text, int text_len, unsigned char *digest);
int i_encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
  unsigned char *iv, unsigned char *ciphertext);
int i_decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key,
  unsigned char *iv, unsigned char *plaintext);



int decode_base64(char * base64_code, char ** out);
int encode_base64(char * base64_code, int len, char ** out);

int decode_base64url(char * src, char ** out);
int encode_base64url(char * src, int len, char ** out);



#ifdef __cplusplus
}
#endif


#endif /* LIB_CRYPTO_H_ */
