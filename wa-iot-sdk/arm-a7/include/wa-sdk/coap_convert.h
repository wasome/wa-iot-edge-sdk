
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#ifndef __WA_COAP_CONVERT__H_
#define __WA_COAP_CONVERT__H_

#include "wa_sdk.h"
#include "coap_ext.h"


#ifdef __cplusplus
extern "C" {
#endif

void wa_setup_coap_request(coap_packet_t *coap_message, unsigned int code,
    char *url, char * query, char *payload, wa_format_type_t format);

void wa_convert_request_to_coap(restful_request_t * request, coap_packet_t *coap_message);
int wa_coap_url_to_addr(char* url, uip_ipaddr_t *addr);

restful_request_t * wa_new_request_from_coap( coap_packet_t *coap_message);

void wa_convert_response_to_coap(restful_response_t * response, coap_packet_t *coap_message);
void wa_convert_coap_to_response( coap_packet_t *coap_message, restful_response_t * response);


#ifdef __cplusplus
}
#endif


#endif
