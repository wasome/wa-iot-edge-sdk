#ifndef __WA_BUS_H_
#define __WA_BUS_H_

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef enum {
	T_Bus_Generial = 0,

	T_Bus_RoT = 1,
	T_Bus_TCP = 2,
	T_Bus_Serials = 3,
	T_Bus_Device = 4    // directly open Linux device file
} wa_bus_type_t;

typedef struct 
{
	char * 	serials_port_name;
	char *  mode;
	int 	baud_rate;
	int 	padding;
}wa_addr_serials_t;

typedef struct 
{
	char * 	ip;
	int 	port;
}wa_addr_IP_t;

typedef struct 
{
	char * 	device_name;
}wa_addr_device_t;


typedef struct 
{
	char * bus_name;
	uint32_t timeout_s;
	uint32_t timeout_us;
    uint32_t close_bus_idle_ms;
	int32_t min_wait_ms;
	union{
		wa_bus_type_t bus_type;  // put it end avoid align issue
		char padding[4];
	};
	union {
		wa_addr_IP_t tcp_addr;
		wa_addr_serials_t serials_addr;
		wa_addr_device_t device_addr;
		char * general_addr;
	};
}wa_bus_info_t;


typedef struct 
{
	char * device_name;
	char * plugin_name;
	char * URI;
	wa_bus_info_t * bus_info;
	char * custom_cfg;
	uint32_t device_ID;
} wa_plugin_device_info_t;

bool wa_bus_compare_info(wa_bus_info_t *info1, wa_bus_info_t *info2);

wa_bus_info_t* wa_find_factory_bus_info_A(const char * bus_name);
wa_bus_info_t* wa_load_bus_config_A(const char * bus_name);
void wa_bus_info_free(wa_bus_info_t * info);
bool check_bus_info_valid(wa_bus_info_t *info);

/*
@brief: load raw content of device configuration for the plugin.
		caller should call free() to free the returned buffer.
*/
char * wa_plugin_load_device_cfg_A(const char * plugin_name);

/*
@brief: parse device info from configuration for the plugin
*/
wa_plugin_device_info_t * wa_plugin_parse_device_info_A(const char * device_cfg, int * device_num);


/*
@brief: load device info from configuration for the plugin
@plugin_name: the plugin name. refer to API for getting the plugin name:
				WA_GetModuleName()
				WA_GetModuleNameFromContext()
@device_num: the number of devices loaded from the configuration
*/
wa_plugin_device_info_t * wa_plugin_load_device_info_A(const char * plugin_name, int * device_num);

/*
@brief: free the device info
*/
void wa_plugin_free_device_info(wa_plugin_device_info_t * devices, int device_num);


#ifdef __cplusplus
}
#endif


#endif