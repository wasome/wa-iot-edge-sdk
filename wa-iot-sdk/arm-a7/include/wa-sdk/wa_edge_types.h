
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#ifndef IDR_MGT_SHARED_LIBS_UTILS_IAGENT_TYPES_H_
#define IDR_MGT_SHARED_LIBS_UTILS_IAGENT_TYPES_H_

#include "cJSON.h"
#include "wa_sdk.h"


#ifdef __cplusplus
extern "C" {
#endif

#define RD_SET_P_PASSIVE      'p'
#define RD_SET_U_QUEUE_MOD    'u'

typedef enum
{
    iReady_To_Connect = 0,
    iError_In_Connection = 1,

    iCloud_Provisioning = 10,

    // socket is connected
    iSocket_Connecting = 98,
    iSocket_Connected ,
    iCloud_Handshaking,
    iReady_For_Work
} cloud_status_e;


typedef struct
{
    int ilink_status;
    char cloud_status[50];
    time_t iagent_start;
    char iagent_id[100];
    char project[100];
} iagent_info_t;


extern bool parse_iagent_info(const char * payload, int payload_len,  iagent_info_t * info);

/*
    rd_assemble_t: the object for constructing resource directory (RD) message
*/
typedef void * rd_assemble_t;

/*  
    @brief  Create a rd_assemble_t object for constructing the RD messsage.
            Posting a RD message to wagent means the program represents a device.
            The normal device can accept read/write on the resource. 
    @param  di: device ID
    @param  dt: device type 
    @param  is_alias: true - the di is unique under the edge platform, 
                      but may not globaly. When wagent connected to the cloud,
                      the device ID reported to cloud will be {gatewayID}_{di},
                      so it can be unique under the cloud. 
                      false - the di is ensured globally unique.
*/
rd_assemble_t rd_data_init_A(const char * di,
        const char * st,
        const char * dt,
        bool is_alias);

/*
    @brief  destroy the rd_assemble_t object
*/
void rd_data_destroy(rd_assemble_t rd);

/*
    @brief  add a resource URI and the resource type into the RD message.
*/
void rd_data_add_resource(rd_assemble_t rd, const char * ri, const char * rt);

/*
    @brief   add a resource which has multiple instances into the RD message
    @param   ri/rd: resource URI and resource type
    @param   instances: array of instance ID, like [1, 2, 3, 5].
                        The ID of resource is {ri}/{instance id}, e.g. /light/1
    @param   inst_num:  the sizeof instances array
*/
void rd_data_add_resource_instances(rd_assemble_t rd,
        const char * ri,
        const char * rt,
        int * instances,
        int inst_num);

/*
    @brief  add resources in cJSON object
    @param  is_rt2:  true - the content of rt is rt2 format, like: 
                    [{name: "property name", type: "integer|bool|string|float" }]
*/
void rd_data_add_resources_cjson(rd_assemble_t rd,
        const char * ri,
        cJSON * rt,
        int * instances,
        int inst_num,
        bool is_rt2);
        
void rd_data_set(rd_assemble_t rd, const char * set);

/*
    @brief  Tell this device only reporting data, then it doesn't support read/write from client
*/
void rd_data_set_passive(rd_assemble_t rd);

/*
    @brief  Tell this device will send RD update message in the seconds given in ttl
    @param  ttl: lifetime in seconds
*/
void rd_data_set_ttl(rd_assemble_t rd, int ttl);

/*
    @brief  Return the text format content of the RD message.
            It is normally used for reporting to the wagent.
*/
char * rd_data_payload_A(rd_assemble_t rd);


/*
    @brief  load all resources with type by calling wa_resource_set_type().
            add them into the rd message.
*/
void wa_load_resources_to_rd(RESTFUL_CONTEXT context, rd_assemble_t rd);



#ifdef __cplusplus
}
#endif
#endif /* IDR_MGT_SHARED_LIBS_UTILS_IAGENT_TYPES_H_ */
