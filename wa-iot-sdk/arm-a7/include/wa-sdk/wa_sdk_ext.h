
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#ifndef __WA__SDK_EXT__H_
#define __WA__SDK_EXT__H_
#include <stdbool.h>
#include <stdint.h>
#include "wa_sdk.h"
#include "agent_core_lib.h"

#ifdef __cplusplus
extern "C" {
#endif


transaction_id_t wa_async_transanction_wait(RESTFUL_CONTEXT restful_context, sync_ctx_t  sync_ctx, 
    transaction_id_t id, WA_Result_Handler result_handler, 
    void* ctx_data, uint32_t timeout);


#ifdef __cplusplus
}
#endif


#endif /* __WA_PLUGIN_SDK__H_ */
