/*
 * Copyright (C) 2017 Intel Corporation.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

// Copyright (c) WanSheng Intelligent Corp. All rights reserved.


#ifndef EXTERNAL_IMRT_SHARED_LIBS_AMS_SDK_AMS_CONSTANTS_H_
#define EXTERNAL_IMRT_SHARED_LIBS_AMS_SDK_AMS_CONSTANTS_H_


///
/// definition for the software products
///
#define SW_IAGENT 	"wa-agent"
#define SW_AMS		"ams"

///
/// definition for the target types
///
#define TT_DEVICE			"device"		        // target id: the id assigned by the software for the device, such as wagent id
#define TT_DEVICE_ON_GW		"device_on_gateway"		// target id: the id of subordinate devices that are connected or managed by this software
#define TT_DEVICE_GROUP		"group"			        // target id: the group id
#define TT_DEVICE_TYPE		"device_type"		    // target id: the name of device type
#define TT_GROUP_TYPE		"group_type"	        // target id: the name of group type
#define TT_RESOURCE_TYPE   	"resource_meta"		    // target id: the name of resource type, such as "oic.light"
#define TT_MODBUS_TYPE   	"modbus_type"	        // target id: the name of resource type, such as "oic.light"
#define TT_PROJECT          "project"

#define TID_UNKNOWN     "_unknown_"

#define TYPE_GENERIC            "software_product"
#define TYPE_RUNTIME_ENGINE     "runtime_engine"
#define TYPE_MANAGED_APP        "managed_app"
#define TYPE_WASM_APP           "wasm_app"


#endif /* EXTERNAL_IMRT_SHARED_LIBS_AMS_SDK_AMS_CONSTANTS_H_ */
