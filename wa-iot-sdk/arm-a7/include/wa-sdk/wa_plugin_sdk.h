
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#ifndef __WA_PLUGIN_SDK__H_
#define __WA_PLUGIN_SDK__H_

#include "plugin_constants.h"
#include "broker.h"
#include "plugin_dlist.h"
#include "plugin_message.h"
#include "wa_sdk.h"
#ifdef __cplusplus
extern "C" {
#endif

BROKER_HANDLE   WA_RestContextGetBroker(RESTFUL_CONTEXT context);

MODULE_HANDLE   WA_RestContextGetModule(RESTFUL_CONTEXT context);

bool WA_SendRequest(RESTFUL_CONTEXT restful_context, 
    restful_request_t * request, 
    WA_Result_Handler response_handler, void * user_data, int timeout_ms);

restful_response_t * WA_SendRequestWait(RESTFUL_CONTEXT restful_context, 
    restful_request_t * request, int timeout_ms);


// Send out the response that was set into the req_env.
// The user request handler doesn't need to call this, 
// as the framework will send out the response.
// If no reponse was set, then framework won't send out response.
// So the caller can clone the req_env and send the response later.
void WA_SendResponse(REQ_ENV_HANDLE req_env);

RESTFUL_CONTEXT WA_InitRestful(BROKER_HANDLE broker, MODULE_HANDLE module);

// register the url that this plugin will serve and handling function
//* url match patterns:
// * sample 1: /abcd, match "/abcd" only
// * sample 2: /abcd/ match match "/abcd" and "/abcd/*"
// * sample 3: /abcd*, match any url started with "/abcd"
// * sample 4: /abcd/*, exclude "/abcd"
void WA_RegisterResource(RESTFUL_CONTEXT context, const char * url, WA_Resource_Handler handler, rest_action_t action);

typedef uint32_t WA_timer_t;
WA_timer_t WA_SetTimer(RESTFUL_CONTEXT context, int timeout_ms, WA_Timer_Handler, void * user_data, bool repeat);

void WA_SetRemotePoster(RESTFUL_CONTEXT restful_context, RestfulRemoteHandler poster, void * arg);

// return the milliseconds to the nearest expiry time
uint32_t WA_RestCheckExpiry(RESTFUL_CONTEXT context);


// return the module name from the gateway configuration file.
const char * WA_GetModuleName(BROKER_HANDLE broker, MODULE_HANDLE module);

// return the module name from restful context.
// it can be the name from gateway configuration file if the context was initiated with NULL name.
const char * WA_GetModuleNameFromContext(RESTFUL_CONTEXT rest_ctx);

// tell gateway if this plugin can be unplugged on the fly
void WA_SetPlugin_Unplugable(BROKER_HANDLE broker, MODULE_HANDLE module, bool unplugbale);



#ifdef __cplusplus
}
#endif


#endif /* __WA_PLUGIN_SDK__H_ */
