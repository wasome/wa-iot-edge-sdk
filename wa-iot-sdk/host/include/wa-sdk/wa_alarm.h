
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#ifndef __IAGENT_ALARM_H__
#define __IAGENT_ALARM_H__

#include <stdlib.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "cJSON.h"
#include "coap_sdk.h"

#ifdef __cplusplus
extern "C"
{
#endif



typedef unsigned int alarm_id_t;
typedef unsigned int event_id_t;


#define URI_ALARM "/alarm"
#define URI_EVENT "/event"

enum
{
    ALARM_SECTION_IAGENT_INTERNAL = 100,
    ALARM_ID_DSS_THRESHOLD,

    ALARM_SECTION_APP_PLC = 10000,
    ALARM_SECTION_MODBUS_BUS = 20000,
    ALARM_SECTION_MODBUS_SLAVE = 21000,
    ALARM_SECTION_MODBUS_BROKER = 22000,

    ALARM_SECTION_ETHERCAT = 23000,

};

enum{
    EVENT_SECTION_CONTROL = 1000,
    EVENT_ID_NEW_CONTROL_SCHEDULE,
    EVENT_ID_CONTROL_REQUEST_SUCCESS,
    EVENT_ID_CONTROL_REQUEST_FAIL,
    EVENT_ID_CONTROL_COMMAND,
    EVENT_ID_CONTROL_NEW_TARGET_STATE,
    EVENT_ID_STATUS_SWITCH,

    EVENT_SECTION_AGENT_GENERIC = 2000,
    EVENT_ID_CLOUD_WRITE_RES,    // 云端执行了资源写操作

    EVT_PLC_BASE = 3000,

    EVT_ETHERCAT_BASE = 4000,

    EVENT_SECTION_MODBUS_BROKER = 5000,

    EVENT_SECTION_APP_MUSHROOM = 10000,
    EVENT_ID_SET_TARGET,
    EVENT_ID_ONE_TIME_SET,

};


//event_status:
typedef enum
{
    EVT_ALARM_CLEAR = 10,
    EVT_ALARM_SET = 1,
    EVT_ALARM_UNDEFINED = -1
} alarm_status_t;


typedef enum
{
    S_Undefined = 0,
    S_Info = 1,
    S_Warn = 2,
    S_Fault = 3
} severity_t;

typedef enum
{
    OT_GROUP = 1,
    OT_DSS = 2,
    OT_DEVICE  = 3,
    OT_IAGENT = 4,
    OT_PLC = 5,
    OT_MODBUS_SERVICE = 6,
    OT_MODBUS_CYCLIC_SLAVE = 7,
    OT_MODBUS_CYCLIC_BUS = 8,
    OT_MODBUS_CYCLIC_BROKER = 9,
    OT_ETHERCAT = 20
} object_type_t;


#define OBJECT_ID_SIZE  32
//
// attention: ensure the size of field align with the DB table defined in
// framework/plugins/DB-service/src/DB.cpp
//
typedef struct
{
    unsigned int alarm_id;
    severity_t severity;
    char title[128];
    object_type_t obj_type;
    char obj_id[OBJECT_ID_SIZE];
    char group[64];
    char content[256];
    char clear_reason[64];
    time_t alarm_time;
    time_t clear_time;
    int alarm_status;
}iagent_alarm_t;

#define COPY_ALARM_FIELD(alarm, field, src) strncpy(alarm->field, src, sizeof(alarm->field)-1)
#define COPY_ALARM_FIELD2(alarm, field) strncpy(alarm->field, field, sizeof(alarm->field)-1)


typedef struct
{
    unsigned int event_id;
    severity_t severity;
    char title[128];
    object_type_t obj_type;
    char obj_id[64];
    char content[1024];
    time_t event_time;
}wa_event_t;


void init_alarm(iagent_alarm_t* alarm, 
        unsigned int alarm_id,
        severity_t severity,
        object_type_t target_type,
        const char * target_id,
        const char * title,
        const char * content);

typedef enum {
    Alarm_Status_None = 0,
    Alarm_Status_Posted = 1,
    Alarm_Status_Cleared
}alarm_post_status_t;

//
// supporting APIs
//
void single_alarm_to_JSON(iagent_alarm_t* alarm, cJSON * elem);
char* serialize_single_alarm(iagent_alarm_t* alarm);
char* serialize_alarms_A( iagent_alarm_t * alarms, int num);
bool parse_single_alarm(cJSON * item, iagent_alarm_t * alarm);
int parse_alarm_payload_A(char * payload, int payload_len, iagent_alarm_t ** alarms_out);
char * clear_alarm_payload_A(unsigned int alarm_id, 
    object_type_t target_type, const char * target_id, 
    const char * clear_reason);



//
//
//     Event API
//
//
void wa_init_event(wa_event_t* event, event_id_t event_id,
        severity_t severity,
        object_type_t target_type,
        const char * target_id,
        const char * title,
        const char * content);

void single_event_to_JSON(wa_event_t* event, cJSON * elem);
bool parse_single_event(cJSON * item, wa_event_t * event);
int parse_event_payload_A(char * payload, int len, wa_event_t ** events_out);
char* serialize_single_event(wa_event_t* event);


#ifdef __cplusplus
}
#endif
#endif