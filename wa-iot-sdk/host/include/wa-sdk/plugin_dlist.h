
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#ifndef APPS_GW_BROKER_PLUGIN_SDK_INCLUDE_PLUGIN_DLIST_H_
#define APPS_GW_BROKER_PLUGIN_SDK_INCLUDE_PLUGIN_DLIST_H_

#include <stdbool.h>
#include "wa_sdk.h"
#ifdef __cplusplus
extern "C" {
#endif


typedef struct dlist_entry_ctx * dlist_entry_ctx_t;
typedef struct dlist_node * dlist_node_t;



typedef enum
{
	T_No_Message,
	T_Message_Handler,
	T_Bus_Message,
	T_Callback,	// callback_t

	T_User_Message = 1000
} E_Msg_Type;


void dlist_post(dlist_entry_ctx_t ,E_Msg_Type, void * messageHandle, RemoteHandler handler);
dlist_node_t dlist_get_process(dlist_entry_ctx_t  link);
dlist_entry_ctx_t  create_dlist();
void free_dlist(dlist_entry_ctx_t  dlist);
bool dlist_node_try_process(dlist_node_t node);

// timeout_ms: <=0 - no wait
dlist_node_t  dlist_get_wait(dlist_entry_ctx_t  link, int timeout_ms);

E_Msg_Type dlist_node_get_msg(dlist_node_t, void ** );
RemoteHandler dlist_node_get_handler(dlist_node_t node);

void dlist_node_free(dlist_node_t  node);


#ifdef __cplusplus
}
#endif


#endif /* APPS_GW_BROKER_PLUGIN_SDK_INCLUDE_PLUGIN_DLIST_H_ */
