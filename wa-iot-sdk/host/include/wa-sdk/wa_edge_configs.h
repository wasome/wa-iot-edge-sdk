
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#ifndef IDR_MGT_SHARED_LIBS_IAGENT_CFGS_H_
#define IDR_MGT_SHARED_LIBS_IAGENT_CFGS_H_

#include <string.h>
#include <time.h>
#include "parson_ext.h"
#include "agent_core_lib.h"
#include "wa_edge_types.h"


#ifdef __cplusplus
extern "C"
{
#endif

#ifndef WA_AGENT
#define WA_AGENT "wa-agent"
#endif


// path: can be NULL
char * api_get_agent_dir(char * path, int len);
char *api_get_group_cfg_path(char *buffer, int buffer_len, char *group, char *cfgname);
char *api_get_device_cfg_path(char *buffer, int buffer_len, char *deviceId);
char *api_get_project_cfg_path(char *buffer, int buffer_len, char *projectId);
bool api_set_reset_iagent_flag(const char * flag);
bool api_remove_reset_iagent_flags();
bool api_check_reset_iagent_flag();


JSON_Value *api_load_group_cfg_A(char *room);
JSON_Value *api_get_group_cs_value(JSON_Value *group_cfg_root, char *key);
JSON_Value *api_get_group_dss_value_A(JSON_Value *group_cfg_root, char *key);
bool api_get_dss_resource_url(char *group, char *dss,  char * url_buffer, int buffer_len);
char * api_get_group_display_name(char *group, char * buffer, int buffer_len);



#ifdef __cplusplus
}
#endif

#endif /* IDR_MGT_SHARED_LIBS_UTILS_IAGENT_INFO_H_ */
