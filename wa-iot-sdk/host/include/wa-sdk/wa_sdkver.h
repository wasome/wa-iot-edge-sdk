#ifndef __WA__SDKVER__H_
#define __WA__SDKVER__H_


#define WA_SDK_INTERFACE_VERSION_CURRENT 2
#define WA_SDK_INTERFACE_VERSION_BACK    1

// return the interface version of the framework
int wa_sdk_get_interface_version(void);

// return the backward compatible version that framework support
int wa_sdk_get_interface_version_back(void);

#endif