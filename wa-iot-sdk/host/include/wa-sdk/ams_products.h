
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#ifndef __ams_products_h__
#define __ams_products_h__

#include "cJSON.h"
#include "path.h"
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

cJSON* load_products_A(bool from_cloud);
cJSON* load_all_products_A();
bool check_runtime_installed(const char * runtime_name,const char * runtime_version);

typedef enum
{
    Ams_Category = 0,
    Ams_Subclass,
    Ams_Install_Path,
    Ams_Install_Src

} ams_product_property_e;
char* ams_load_product_property_A(const char * product,const char * version, ams_product_property_e property);


typedef enum
{
    Ams_Category_SW,
    Ams_Category_Runtime,
    Ams_Category_Managed_App,
    Ams_Category_Docker,
    ms_Category_Unknown
    
} ams_category_e;

ams_category_e ams_get_product_category(const char*);

#ifdef __cplusplus
}
#endif



#endif