
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#ifndef PLUGIN_SDK_INCLUDE_MESSAGE_H_
#define PLUGIN_SDK_INCLUDE_MESSAGE_H_

#include "wa_sdk.h"
#include "message.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _bus_event
{
    int payload_fmt;
    int payload_len;
    char * payload;
    char * url;
    char * query;
    char * src_module;
    char * tm;
}bus_event_t;


// refer to the data from messageHandle
bool wa_decode_ref_request(MESSAGE_HANDLE messageHandle, restful_request_t * request);
bool wa_decode_ref_response(MESSAGE_HANDLE messageHandle, restful_response_t * response);


MESSAGE_HANDLE wa_encode_response(restful_response_t * response);
MESSAGE_HANDLE wa_encode_request(restful_request_t * request);

bool decode_event(MESSAGE_HANDLE messageHandle, bus_event_t * event);
MESSAGE_HANDLE encode_event(bus_event_t * event);

#ifdef __cplusplus
}
#endif


#endif /* PLUGIN_SDK_INCLUDE_PLUGIN_BASE_H_ */
