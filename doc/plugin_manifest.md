# 插件清单文件格式说明

## 基本概念

一个插件安装包可以包含若干名为libxxxx.so的插件文件，同时在根路径下包含一个`plugin.manifest`清单文件。描述各个插件的关键信息。


## 示例
```
[{
		"name": "modbus",
		"plugin_file": "libmodbus_server.so",
		"provider": "Beijing WanSheng Intelligent Corp",
		"version": "1.0.0",
		"desc": "modbus",
		"device_use": {
			"support": true,
			"addr_type_support": ["SERIAL_BUS", "IP_ADDR"],
			"require_slave_id": true,
			"custom_fields": [{
				"field": "coding",
				"tip": "coding methods",
				"regex": "",
				"mandatory": false,
				"choices": ["abcd", "cdab", "badc", ""]
			}]			
		},
		"plugin_cfg_fields": [{
			"field": "coding",
			"tip": "coding methods",
			"regex": "",
			"mandatory": false,
			"choices": ["abcd", "cdab", "badc", ""]
		}],

		"resources": [{
				"uri": "/modbus",
				"category": "STANDARD",
				"protocol": "modbus",
				"desc": "access the modbus devices",
				"access": ["PUT", "GET"],
				"query": [],
				"format": ["OCF"]
			},
			{
				"uri": "/mb/{SERIAL_BUS}/{SLAVE_ID}/{REG_ADDR}/{REG_TYPE}",
				"category": "TAG",
				"query": ["items", "coding"],
				"desc": "Using serial bus name and slave ID to access the modbus registers",
				"access": ["PUT", "GET"],
				"tips": [{
					"field": "coding",
					"tip": "coding methods",
					"regex": "",
					"mandatory": false,
					"choices": ["abcd", "cdab", "badc", ""]
				}]
			},
			{
				"uri": "/mb2",
				"category": "TAG",
				"query": ["items", "coding", "area", "reg", "DI"],
				"desc": "Using device name to access the modbus registers",
				"access": ["PUT", "GET"],
				"tips": [{
					"field": "coding",
					"tip": "coding methods",
					"regex": "",
					"mandatory": false,
					"choices": ["abcd", "cdab", "badc", ""]
				}, {
					"field": "DI",
					"tip": "device id",
					"regex": "",
					"mandatory": true
				}, {
					"field": "area",
					"tip": "modbus register area",
					"regex": "",
					"mandatory": true,
					"choices": ["Coil", "Descrete", "Input", "Holding"]
				}]
			},
			{
				"uri": "/mb/bus",
				"category": "GENERIC",
				"desc": "configure the bus used for modbus",
				"access": ["PUT", "GET", "DELETE"],
				"query": [],
				"format": ["JSON"],
				"schema": "show the JSON schema",
				"example": ""
			}
		]
	},

	{
		"name": "lwm2m",
		"plugin_file": "liblwm2m_server.so",
		"provider": "Beijing WanSheng Intelligent Corp",
		"version": "1.0.0",
		"desc": "lwm2m plugin",
		"resources": [{
			"uri": "/lwm2m",
			"category": "STANDARD",
			"protocol": "lwm2m",
			"desc": "access the lwm2m standard devices",
			"access": ["PUT", "GET", "OBS"],
			"format": ["LWM2M"]
		}]
	},

	{
		"name": "database",
		"plugin_file": "libdb_service.so",
		"provider": "Beijing WanSheng Intelligent Corp",
		"version": "1.0.0",
		"desc": "alarm, event and offline data plugin",
		"resources": [{
				"uri": "/alarm",
				"category": "GENERIC",
				"desc": "alarm",
				"access": ["PUT", "GET", "DELETE"],
				"query": [],
				"format": ["JSON"],
				"schema": "show the JSON schema",
				"example": ""
			},
			{
				"uri": "/event",
				"category": "GENERIC",
				"desc": "event",
				"access": ["PUT", "GET", "DELETE"],
				"query": [],
				"format": ["JSON"],
				"schema": "show the JSON schema",
				"example": ""
			}
		]
	}
]
```

## 清单格式说明

顶层是JSON数组，每一个数组元素代表一个插件文件。

```
[{
    "name": "MODBUS",
    "plugin_file": "libmodbus.so",
    "provider": "Beijing WanSheng Intelligent Corp",
    "version": "1.0.0",
    "desc": "modbus",
	"device_use": {},
	"plugin_cfg_fields":[],
    "resources": [ 
        {
            "uri": "/modbus",
            "category": "STANDARD",
            ... ...
        }
    ]
}]
```

### 配置模板

当用户安装了插件后，WebConsole将使用清单中device_use和plugin_cfg_fields字段提供的配置模板，为用户生成该插件的配置界面。

用户在配置界面上输入的数据，生成的配置内容(JSON格式)，将在加载插件过程中提供给插件使用。

- **device_use**字段：描述插件是否支持用于创建虚拟设备。当字段`support`为true表示支持。
  - addr_type_support： 创建虚拟设备时，支持哪些设备地址的配置方式
  - require_slave_id：是否需要为虚拟设备配置从站ID
  - custom_fields：插件自己提供的配置子项的描述

- **plugin_cfg_fields**: 插件全局性的配置模板。


```
		"device_use": {
			"support": true,
			"addr_type_support": ["SERIAL_BUS", "IP_ADDR"],
			"require_slave_id": true,
			"custom_fields": [{
				"field": "coding",
				"tip": "coding methods",
				"regex": "",
				"mandatory": false,
				"choices": ["abcd", "cdab", "badc", ""]
			}]			
		},
		"plugin_cfg_fields": [{
			"field": "coding",
			"tip": "coding methods",
			"regex": "",
			"mandatory": false,
			"choices": ["abcd", "cdab", "badc", ""]
		}],
```

### 关键字段`resources`
每个插件相关对象包含一个关键字段`resources`，是一个JSON数组，其中每个数组元素代表插件提供的一个由URI所代表的服务。URI可以是以下类别(`category`)之一，每一种类别在URI，QUERY和数据格式方面有不同的定义：
- **standard (设备标准)**：基于OCF规范访问某种标准的设备。
- **device (设备类型)**： 基于OCF规范访问某种特定类型的设备。
- **tag (点位)**： 用于访问设备的寄存器（点位）
- **generic (通用)**：一般性的URI，插件可以任意使用URI

### URI类别：standard或者device
- standard URI：遵循格式：{协议名}/{设备ID}/{资源ID}。资源ID可以是符合URI格式的任意定义。
- device URI：遵循格式：{设备类型名}/{设备ID}/{资源ID}。一个设备类型包含的一组预定好的资源ID名。

- standard和device使用相同的QUERY和数据格式定义：
    - QUERY：仅支持名为`pt`查询，表示查询资源下的特定属性(点位)的数据
    - 数据格式：遵守标准定义，可以是OCF，或者LWM2M


```
{
    "uri": "/modbus",
    "category": "STANDARD",
    "protocol": "modbus",
    "desc": "access the modbus devices",
    "access": ["PUT", "GET"],
    "format": ["OCF"]
}
```
- API URI path: 例如/modbus/device100/flow，其中：
    - modbus：为本资源的`uri`字段内容
    - device100: 设备ID
    - flow: 资源ID
- format:
  - OCF： 遵守OCF定义的JSON格式，一般为包含若干键值对的JSON对象，其中键值为资源属性名。
  - LWM2M： 遵守LWM2M定义的JSON格式
  - JSON: 一般的JSON格式，格式的具体描述需要借助schema和example字段，具体见GENEIRC类别的示例。

### URI类别：tag
- URI：可以是任意定义格式,如果有用`{}`包含的字段，表示动态变量，是调用者来输入的。
- QUERY：可以是任意格式
  
- tips字段：描述每一个URI输入字段和query的用户提示信息：
```
{
	"field": "name",
	"tip": "device id",
	"regex": "",
	"mandatory": true
}
```

**预留字**  
- {DI}: 表示DEVICE ID。如果URI PATH中包含{DI}，调用者应该在这个字段中填入目标设备的ID。否则，QUERY中应该支持关键字DI=xxx。



**数据格式**  
点位类别URI应该遵守预定义的数据格式：
  - 当查询单个点位，返回数据为TEXT格式，如：1.0
  - 当查询多个连续点位，返回数据为JSON数组，如：[10, 20]



### URI类别：generic
- URI， QUERY，数据格式：均可以是任意格式

```
    {
        "uri": "/mb/bus/[bus]",
        "category": "GENERIC",
        "desc": "configure the bus used for modbus",
        "access": ["PUT", "GET", "DELETE"],
        "query": [],
        "tips": [
        ],        
        "format": ["JSON"],
        "schema": "show the JSON schema",
        "example": ""
    },
```




### URI类别： DEVICE
例子：
```
{
    "uri": "/xxx.001",
    "category": "DEVICE",
	"builtin_device_id": ["d01"],
    "desc": "xxx devices,
    "access": ["PUT", "GET"],
    "format": ["OCF"],
	"device_type": "",
	"device_resources":[
		{
			"res_name": "/humidity",
			"res_type":"private",
			"res_define":
			{
				"name":"value",
				"type":"number"
			}
		}
	]	
}
```

插件将提供标准的OCF或者LWM2M设备访问能力：
- URI使用说明：调用者遵循格式`{设备类型名}/{设备ID}/{资源ID}`。上面的例子中，调用者访问的URI如：/xxx.001/d01/humidity。其中各个字段：
  - xxx.001：设备型号
  - d01: 设备ID
  - humidity： 资源ID名
- builtin_device_id: 如果插件使用的设备ID是预先固定好的，本字段保存预定的设备ID。
- device_type： 一个设备类型包含的一组预定好的资源ID名。如果不存在，将使用字段device_resources来描述本设备类型的包含资源。
- res_type： 本资源的类型定义名称，一般可以是OCF或者OMA组织定义好的资源模型名称。如果res_type没有提供，表示一个自定义类型的资源。
- device_resources： 本字段描述本资源的类型自定义。