#Copyright (c) WanSheng Intelligent. All rights reserved.
#Licensed under the MIT license. See LICENSE file in the project root for full license information.

# Note:
# This is cmake is for static including all source files under the WA-SDK (without reference to the AZURE IOT EDGE)
#

cmake_minimum_required(VERSION 2.8.12)

SET(COMMON_C_CXX_FLAGS " -D_GNU_SOURCE  -fPIC " )
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS}  ${COMMON_C_CXX_FLAGS} --std=c99 -Werror=implicit-function-declaration")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${COMMON_C_CXX_FLAGS} -std=c++0x -Wno-literal-suffix")

set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g2 -ggdb ")  
set(CMAKE_CXX_FLAGS_RELEASE "-Os -Wall")

set(CMAKE_C_FLAGS_DEBUG "-O0 -g2 -ggdb ")  
set(CMAKE_C_FLAGS_RELEASE "-Os -Wall")

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wl,--no-undefined -Wl,--no-allow-shlib-undefined -Wl,--copy-dt-needed-entries")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wl,--no-undefined -Wl,--no-allow-shlib-undefined -Wl,--copy-dt-needed-entries")
set(CMAKE_LINKER_FLAGS "${CMAKE_LINKER_FLAGS} --no-undefined --no-allow-shlib-undefined --as-needed --warn-unresolved-symbols")


SET(WA_SDK_REPO_DIR ${CMAKE_CURRENT_LIST_DIR}/..)
SET(SHARED_LIBS_DIR ${WA_SDK_REPO_DIR}/idr-mgt-shared-libs)

message (STATUS "CMAKE_BUILD_TYPE: ${CMAKE_BUILD_TYPE}")

include(${WA_SDK_REPO_DIR}/ams/ex_ams.cmake)
include(${WA_SDK_REPO_DIR}/internal-shared/ilink/ilink_lib.cmake)

include(${WA_SDK_REPO_DIR}/idr-mgt-shared-libs/lib/ssg_lib.cmake)
include(${WA_SDK_REPO_DIR}/idr-mgt-shared-libs/utils/ssg_utils.cmake)
include(${WA_SDK_REPO_DIR}/idr-mgt-shared-libs/crypto/crypto_lib.cmake)
include(${WA_SDK_REPO_DIR}/idr-mgt-shared-libs/coap/ex_coap.cmake)

include(${WA_SDK_REPO_DIR}/app-sdk/c-api/common/iagent_common.cmake)
include(${WA_SDK_REPO_DIR}/app-sdk/c-api/client-api.cmake)

include(${WA_SDK_REPO_DIR}/external/hiredis.cmake)
include(${WA_SDK_REPO_DIR}/external/util_deps.cmake)

include(${WA_SDK_REPO_DIR}/core-sdk/core_sdk.cmake)

SET(WA_SDK_BASE_LB_SRC
    ${SSG_LIB_SOURCE}
    ${EX_PATH_SOURCE}
    ${EX_AMS_SOURCE}
    ${SSG_UTILS_SOURCE_FULL}
    ${IAGENT_COMMON_SOURCE}
    ${EX_COAP_SOURCE}
    ${SOURCE_HIREDIS}
    ${IAGENT_CLIENT_API_SRC}
    ${WA_CORE_SDK_SRC}
)

add_definitions(-DRUN_ON_LINUX)

