#Copyright (c) WanSheng Intelligent. All rights reserved.
#Licensed under the MIT license. See LICENSE file in the project root for full license information.

cmake_minimum_required(VERSION 2.8.12)

SET(COMMON_C_CXX_FLAGS " -D_GNU_SOURCE  -fPIC " )
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS}  ${COMMON_C_CXX_FLAGS} --std=c99 -Werror=implicit-function-declaration")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${COMMON_C_CXX_FLAGS} -std=c++0x -Wno-literal-suffix")

set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g2 -ggdb ")  
set(CMAKE_CXX_FLAGS_RELEASE "-Os -Wall")

set(CMAKE_C_FLAGS_DEBUG "-O0 -g2 -ggdb ")  
set(CMAKE_C_FLAGS_RELEASE "-Os -Wall")

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wl,--no-undefined -Wl,--no-allow-shlib-undefined -Wl,--copy-dt-needed-entries")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wl,--no-undefined -Wl,--no-allow-shlib-undefined -Wl,--copy-dt-needed-entries")
set(CMAKE_LINKER_FLAGS "${CMAKE_LINKER_FLAGS} --no-undefined --no-allow-shlib-undefined --as-needed --warn-unresolved-symbols")


SET(PLUGIN_SDK_DIR ${CMAKE_CURRENT_LIST_DIR})
SET(SHARED_LIBS_DIR ${CMAKE_CURRENT_LIST_DIR}/../idr-mgt-shared-libs)


message (STATUS "CMAKE_BUILD_TYPE: ${CMAKE_BUILD_TYPE}")

if(NOT DEFINED platform)
  set(platform "host")
  message(STATUS "common.cmake: platform  not defined. set it to: \"${platform}\"")
else()
  message(STATUS "common.cmake: platform is [${platform}]")
endif()

# Set the install prefix
if(NOT DEFINED PLUGIN_LIB_BASE_DIR)
  set(PLUGIN_LIB_BASE_DIR ${PLUGIN_SDK_DIR}/../out/${platform}/plugin_sdk)
  message(STATUS "common.cmake: PLUGIN_LIB_BASE_DIR not defined. set it to: \"${PLUGIN_LIB_BASE_DIR}\"")
endif()

include(${PLUGIN_SDK_DIR}/../ams/ex_ams.cmake)
include(${PLUGIN_SDK_DIR}/../internal-shared/ilink/ilink_lib.cmake)

include(${PLUGIN_SDK_DIR}/../idr-mgt-shared-libs/lib/ssg_lib.cmake)
include(${PLUGIN_SDK_DIR}/../idr-mgt-shared-libs/utils/ssg_utils.cmake)
include(${PLUGIN_SDK_DIR}/../idr-mgt-shared-libs/crypto/crypto_lib.cmake)

if(DEFINED XX_EXCLUDE_COAP)
  message(STATUS "common.cmake: exclude include the ex_coap.cmake")
else()
  include(${PLUGIN_SDK_DIR}/../idr-mgt-shared-libs/coap/ex_coap.cmake)
endif()


include(${PLUGIN_SDK_DIR}/../app-sdk/c-api/common/iagent_common.cmake)
include(${PLUGIN_SDK_DIR}/../external/hiredis.cmake)
include(${PLUGIN_SDK_DIR}/../external/util_deps.cmake)

include_directories(${PLUGIN_SDK_DIR}/../core-sdk/include)


SET(SRC_PLUGIN_COMMON
    ${SSG_LIB_SOURCE}
    ${EX_PATH_SOURCE}
    ${EX_AMS_SOURCE}
    ${SSG_UTILS_SOURCE_FULL}
    ${IAGENT_COMMON_SOURCE}
    ${EX_COAP_SOURCE}
    ${SOURCE_HIREDIS}
)

add_definitions(-DRUN_ON_LINUX -DRUN_AS_BROKER_MODULE -DBUILD_PLATFORM=${platform})

include_directories(${PLUGIN_SDK_DIR}/include)


include_directories(${PLUGIN_LIB_BASE_DIR}/include)
include_directories(${PLUGIN_LIB_BASE_DIR}/include/idrm)
include_directories(${PLUGIN_LIB_BASE_DIR}/include/azure_iot_gateway_sdk-1.0.8)
include_directories(${PLUGIN_LIB_BASE_DIR}/include/azureiot)
include_directories(${PLUGIN_LIB_BASE_DIR}/include/umock_c)


link_directories(${PLUGIN_LIB_BASE_DIR}/lib)
link_directories(${PLUGIN_LIB_BASE_DIR}/lib/azure_iot_gateway_sdk-1.0.8)


function(linkSharedUtil whatIsBuilding)
  target_link_libraries(${whatIsBuilding}  gateway aziotsharedutil nanomsg pthread dl rt m)
endfunction(linkSharedUtil)
