
#// -fpack-struct=4

SET(COMMON_C_CXX_FLAGS " -D_GNU_SOURCE  -fPIC " )
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS}  ${COMMON_C_CXX_FLAGS} --std=c99  -Werror=implicit-function-declaration")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${COMMON_C_CXX_FLAGS} -std=c++0x  -Wno-literal-suffix")

set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g2 -ggdb ")  
set(CMAKE_CXX_FLAGS_RELEASE "-Os -Wall")

set(CMAKE_C_FLAGS_DEBUG "-O0 -g2 -ggdb ")  
set(CMAKE_C_FLAGS_RELEASE "-Os -Wall")

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wl,--no-undefined -Wl,--no-allow-shlib-undefined -Wl,--copy-dt-needed-entries")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wl,--no-undefined -Wl,--no-allow-shlib-undefined -Wl,--copy-dt-needed-entries")
set(CMAKE_LINKER_FLAGS "${CMAKE_LINKER_FLAGS} --no-undefined --no-allow-shlib-undefined --as-needed --warn-unresolved-symbols")
