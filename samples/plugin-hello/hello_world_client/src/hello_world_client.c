
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include "hello_world.h"

int client_WorkingThread(void *param)
{
    E2E_MODULE_DATA* module_data = (E2E_MODULE_DATA*)param;
    static int mid = 0;

    while(1)
    {
        sleep(5);

        restful_request_t request = {0};
        tick_time_t * user_data = (tick_time_t *) malloc(sizeof(tick_time_t));
        *user_data = bh_get_tick_us();
        request.action = T_Get;
        request.url = "/hello";

        printf("client: sending the request. REST-CTX=%p\n", module_data->restful_context);
        WA_SendRequest(module_data->restful_context, &request, hello_response_callback, user_data, 3);
    }

    return 0;
}


static void E2EModule_Receive(MODULE_HANDLE moduleHandle, MESSAGE_HANDLE messageHandle)
{
    (void)moduleHandle;
    (void)messageHandle;
    return;
}

static void E2EModule_Destroy(MODULE_HANDLE moduleHandle)
{
    if (moduleHandle == NULL)
    {
        WARNING2("Attempt to destroy NULL module");
    }
    else
    {
        E2E_MODULE_DATA* module_data = (E2E_MODULE_DATA*)moduleHandle;
        free(module_data);
    }
}

static void E2EModule_Start(MODULE_HANDLE module)
{
    E2E_MODULE_DATA* module_data = (E2E_MODULE_DATA*)module;

    RESTFUL_CONTEXT ctx = WA_InitRestful(module_data->broker, module);
    module_data->restful_context = ctx;
    ThreadAPI_Create(&module_data->threadHandle, client_WorkingThread, module_data);

#ifdef WA_THREAD_DEMO
    ThreadAPI_Create(&module_data->threadHandle, client_HandlingThread, module_data);
    WA_SetRemotePoster(ctx, job_poster, module_data);
    printf("client: dedicated thread for callback created!!\n");
#endif
}

static MODULE_HANDLE E2EModule_Create(BROKER_HANDLE broker, const void* configuration)
{
    E2E_MODULE_DATA * result;
    if (broker == NULL)
    {
        WARNING2("Helo client: invalid Fake E2E module args.");
        result = NULL;
    }
    else
    {
        /* allocate module data struct */
        result = (E2E_MODULE_DATA*)malloc(sizeof(E2E_MODULE_DATA));
        if (result == NULL)
        {
            WARNING2("couldn't allocate memory for E2E Module");
        }
        else
        {
            /* save the message broker */
            memset(result, 0, sizeof(*result));
            result->broker = broker;
            result->internal_queue = create_dlist();
        }
    }
    return result;
}


static void* HelloWorld_ParseConfigurationFromJson(const char* configuration)
{
	(void)configuration;
    if(configuration)
    {
        printf("HelloWorld_ParseConfigurationFromJson: %s\n", configuration);
    }
    return NULL;
}

static void HelloWorld_FreeConfiguration(void* configuration)
{
	(void)configuration;
}


/*
 *    Required for all modules:  the public API and the designated implementation functions.
 */
static const MODULE_API_1 E2EModule_APIS_all =
{
    {MODULE_API_VERSION_1},

    HelloWorld_ParseConfigurationFromJson,
	HelloWorld_FreeConfiguration,
    E2EModule_Create,
    E2EModule_Destroy,
    E2EModule_Receive,
    E2EModule_Start

};

MODULE_EXPORT const MODULE_API* Module_GetApi(MODULE_API_VERSION gateway_api_version)
{
    if(WA_SDK_INTERFACE_VERSION_CURRENT < wa_sdk_get_interface_version_back())
    {
        printf("HelloWorld: WA_SDK_INTERFACE_VERSION_CURRENT (%d) < framework supported (%d)\n",
            WA_SDK_INTERFACE_VERSION_CURRENT, wa_sdk_get_interface_version_back());
        return NULL;
    }
    (void)gateway_api_version;
    return (const MODULE_API *)&E2EModule_APIS_all;
}
