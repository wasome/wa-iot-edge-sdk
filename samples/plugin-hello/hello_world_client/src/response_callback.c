
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#include "hello_world.h"
#include "agent_core_lib.h"
#include "wa_plugin_sdk.h"

void hello_response_callback2(RESTFUL_CONTEXT context, restful_response_t *response, void * user_data)
{
	printf("hello_response_callback2: code=%d\n",  response?response->code:-1);
}


int cnt = 0;
void hello_response_callback(RESTFUL_CONTEXT context, restful_response_t *response, void * user_data)
{
	tick_time_t * req_time = (tick_time_t *)user_data;

	if(response == NULL)
		printf("hello_response_callback: timeout!\n");
	else if(response->payload)
		printf("hello_response_callback payload: %.*s\n", response->payload_len, response->payload);
	printf("\tctx=%p,code=%d, microseconds since requesting: %ld\n", 
		context, response?response->code:-1, bh_get_tick_us() - *req_time);


	free(user_data);

	restful_request_t request = {0};
	char buf[100];
	request.action = T_Put;
	request.url = "/hello";
	request.payload=buf;
	snprintf(buf, sizeof(buf),"cnt=%d", cnt++);
	request.payload_len = strlen(buf);

	//WA_SendRequest(context, &request, hello_response_callback2, NULL, 10);	

}
