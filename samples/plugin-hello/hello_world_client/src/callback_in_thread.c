#include "hello_world.h"



void job_poster (restful_remote_job_t job, RemoteHandler job_handler, void * arg_for_poster)
{
    E2E_MODULE_DATA* module_data = (E2E_MODULE_DATA*)arg_for_poster;
    dlist_post(module_data->internal_queue, T_Message_Handler, job, job_handler);
}

int client_HandlingThread(void *param)
{
    E2E_MODULE_DATA* module_data = (E2E_MODULE_DATA*)param;
    
    while(1)
    {
        dlist_node_t job = dlist_get_wait(module_data->internal_queue, 10);
        if(job)
        {
            if(dlist_node_try_process(job))
                printf("job processed in thread client_HandlingThread\n");
            else
            {
                printf("failed to process job!\n");
                dlist_node_free(job);
            }
        }

    }
    return 0;
}
