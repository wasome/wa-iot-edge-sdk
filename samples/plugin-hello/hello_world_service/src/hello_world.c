
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#define LOG_TAG "HELLO-SVC"
#include "hello_service.h"

#ifdef WA_THREAD_DEMO

void job_poster (restful_remote_job_t job, RemoteHandler job_handler, void * arg_for_poster)
{
    E2E_MODULE_DATA* module_data = (E2E_MODULE_DATA*)arg_for_poster;
    dlist_post(module_data->internal_queue, T_Message_Handler, job, job_handler);
}


int service_HandlingThread(void *param)
{
    E2E_MODULE_DATA* module_data = (E2E_MODULE_DATA*)param;
    
    while(1)
    {
        dlist_node_t job = dlist_get_wait(module_data->internal_queue, 10);
        if(job)
        {
            if(!dlist_node_try_process(job))
            {
                WARNING2("failed to process job!");
                dlist_node_free(job);
            }
            else
            {
                printf("job processed in thread service_HandlingThread\n");
            }
                
        }

    }
    return 0;
}

#endif