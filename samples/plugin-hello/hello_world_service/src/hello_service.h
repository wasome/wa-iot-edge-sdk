
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#ifndef HELLO_WORLD_H
#define HELLO_WORLD_H

#include "module.h"
#include <pthread.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>
#include <printf.h>
#include <errno.h>
#include <sys/stat.h>
#include <linux/prctl.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/prctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "agent_core_lib.h"
#include "wa_plugin_sdk.h"
#include "azure_c_shared_utility/threadapi.h"

#ifdef __cplusplus
extern "C"
{
#endif


typedef struct E2E_MODULE_DATA_TAG
{
    BROKER_HANDLE       broker;
    RESTFUL_CONTEXT     restful_context;
    dlist_entry_ctx_t   internal_queue;
    THREAD_HANDLE       threadHandle;

} E2E_MODULE_DATA;




extern void res_hello_handler_get (restful_request_t *request, REQ_ENV_HANDLE req_env);
extern void res_hello_handler_put (restful_request_t *request, REQ_ENV_HANDLE req_env);

void register_services(RESTFUL_CONTEXT framework);


int service_HandlingThread(void *param);
void job_poster (restful_remote_job_t job, RemoteHandler job_handler, void * arg_for_poster);

#ifdef __cplusplus
}
#endif



#endif /*HELLO_WORLD_H*/
