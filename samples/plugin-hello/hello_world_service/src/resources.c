
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#define LOG_TAG "HELLO"

#include <stdlib.h>
#include "er-coap-constants.h"
#include "hello_service.h"


static char * g_content = NULL;
static int g_content_len = 0;
static int g_content_fmt = IA_TEXT_PLAIN;

void res_hello_handler_get (restful_request_t *request, REQ_ENV_HANDLE req_env)
{
    int payload_len;
    char * payload;
	if(g_content_len)
	{
        payload = g_content;
        payload_len = g_content_len;
	}
	else
	{
	    payload = "hello";
	    payload_len = strlen(payload);
	}

    wa_set_response(req_env, CONTENT_2_05, g_content_fmt, payload_len, payload);

	TraceI(1, "hello service get: returned %d bytes\n", payload_len);
	return;
}



void res_hello_handler_put (restful_request_t *request, REQ_ENV_HANDLE req_env)
{
    if(g_content) free(g_content);
    g_content = NULL;
    g_content_len = 0;

    if(request->payload_len > 0)
    {
        g_content = malloc(request->payload_len);
        memcpy(g_content, request->payload, request->payload_len);
        g_content_len = request->payload_len;
        g_content_fmt = request->payload_fmt;
    }

    wa_set_response_status(req_env, CHANGED_2_04);

    LOGX( "hello service put:  %d bytes\n", g_content_len);
}
