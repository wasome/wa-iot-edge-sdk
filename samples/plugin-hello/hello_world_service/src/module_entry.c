
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#define LOG_TAG "hello"
#include "hello_service.h"


static void E2EModule_Receive(MODULE_HANDLE moduleHandle, MESSAGE_HANDLE messageHandle)
{
    (void)moduleHandle;
    (void)messageHandle;
    return;
}

static void E2EModule_Destroy(MODULE_HANDLE moduleHandle)
{
    if (moduleHandle == NULL)
    {
        WARNING2("Attempt to destroy NULL module");
    }
    else
    {
        E2E_MODULE_DATA* module_data = (E2E_MODULE_DATA*)moduleHandle;
        free(module_data);
    }
}

static void E2EModule_Start(MODULE_HANDLE module)
{
    E2E_MODULE_DATA* module_data = (E2E_MODULE_DATA*)module;

    RESTFUL_CONTEXT ctx = WA_InitRestful(module_data->broker, module);
    WA_RegisterResource(ctx, "/hello", res_hello_handler_get, T_Get);
    WA_RegisterResource(ctx, "/hello", res_hello_handler_put, T_Put);
    module_data->restful_context = ctx;

#ifdef WA_THREAD_DEMO
    WA_SetRemotePoster(ctx, job_poster, module_data);
    ThreadAPI_Create(&module_data->threadHandle, service_HandlingThread, module_data);
#endif
}

static MODULE_HANDLE E2EModule_Create(BROKER_HANDLE broker, const void* configuration)
{
    E2E_MODULE_DATA * result;
    if (broker == NULL)
    {
        WARNING2("Hello service: invalid Fake E2E module args.");
        result = NULL;
    }
    else
    {
        /* allocate module data struct */
        result = (E2E_MODULE_DATA*)malloc(sizeof(E2E_MODULE_DATA));
        if (result == NULL)
        {
            WARNING2("couldn't allocate memory for E2E Module");
        }
        else
        {
            /* save the message broker */
            memset(result, 0, sizeof(*result));
            result->broker = broker;
            result->internal_queue = create_dlist();
        }
    }
    return result;
}

/*
 *    Required for all modules:  the public API and the designated implementation functions.
 */
static const MODULE_API_1 HelloSvcModule_APIS_all =
{
    {MODULE_API_VERSION_1},
    NULL,
	NULL,
    E2EModule_Create,
    E2EModule_Destroy,
    E2EModule_Receive,
    E2EModule_Start

};

MODULE_EXPORT const MODULE_API* Module_GetApi(MODULE_API_VERSION gateway_api_version)
{
    (void)gateway_api_version;
    if(WA_SDK_INTERFACE_VERSION_CURRENT < wa_sdk_get_interface_version_back())
    {
        printf("HelloWorld: WA_SDK_INTERFACE_VERSION_CURRENT (%d) < framework supported (%d)\n",
            WA_SDK_INTERFACE_VERSION_CURRENT, wa_sdk_get_interface_version_back());
        return NULL;
    }    
    return (const MODULE_API *)&HelloSvcModule_APIS_all;
}
