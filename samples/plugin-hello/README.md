# 插件Hello world开发示例

## 快速上手
### 编译帮助
```
./build_hello.sh -h
```

### 编译
运行脚本：
```
./build_hello.sh 
```

### 打包
运行：
```
./build_hello.sh -a
```


## 运行

1. 进入[`samples/simple-edge-broker/`](../simple-edge-broker/)
2. 运行：
   ```
    ./build_broker.sh run
   ```


## 在特定线程中执行回调函数

运行：
```
./build_hello.sh -t
```
启动
