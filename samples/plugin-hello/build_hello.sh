#!/bin/bash

#Copyright (c) Beijing Wansheng Intelligent Corp. All rights reserved.
#Licensed under the MIT license. See LICENSE file in the project root for full license information.


SCRIPT=$(readlink -f "$0")
script_path=$(cd "$(dirname "$0")/" && pwd)
samples_dir=$(readlink -f "$script_path/..")
repo_root_dir=$script_path/../..
platforms_dir=$repo_root_dir/scripts/platforms
echo platforms_dir=[$platforms_dir]
platform=host
clean=
pack=
usage ()
{
    echo "[options]"
    echo " -h                                       print help infomation"
    echo " -c                                       clean build"
    echo " -a                                       pack"
    echo " -t                                       enable WA_THREAD_DEMO in build"
    echo " -p [host | arm-a7]                       set target toolchain"

    exit 1
}

while getopts "tcahp:" arg
do
    case $arg in
        p)
        platform=$OPTARG;;
        a)
        pack="yes";;        
        t)
        export WA_THREAD_DEMO="true"
        echo "WA_THREAD_DEMO=true";;
        c)
        clean="yes";;
        h)
        usage;;
        ?)
        echo $arg
        usage;;
    esac
done

if [ "$platform" != "host" ]; then
    [ -f $platforms_dir/$platform/toolchain.cmake ] || echo "toolchain.cmake is not present under  $platforms_dir/$platform/" || exit 1
    toolchain_def="-DCMAKE_TOOLCHAIN_FILE=$platforms_dir/$platform/toolchain.cmake"
    setenv_file=$platforms_dir/$platform/setenv.sh
fi


if [ -z "$WA_SDK_DIR" ]; then
    [ -d "$samples_dir/../out/$platform/wa-iot" ] && export WA_SDK_DIR="$samples_dir/../out/$platform/wa-iot"
    # set for distribution
    [ -d "$samples_dir/../wa-iot-sdk/$platform" ] && export WA_SDK_DIR="$samples_dir/../wa-iot-sdk/$platform"
fi

if [ -z "$WA_SDK_DIR" ]; then
    echo "Please set WA_SDK_DIR to the path of WA-SDK toolchains, exit.." && exit 1
else
    [ ! -d $WA_SDK_DIR ] && echo "WA_SDK_DIR $WA_SDK_DIR not present, exit.." && exit 1
fi

[ ! -f $WA_SDK_DIR/wa-sdk.cmake ] && echo "No wa-sdk.cmake file under WA_SDK_DIR [$WA_SDK_DIR]. exit.." && exit 1


CMAKE_WA_SDK_DIR="-DWA_SDK_DIR=$WA_SDK_DIR"
echo "WA_SDK_DIR=$WA_SDK_DIR"

build_dir=$script_path/out
[ -d $build_dir ] || mkdir -p $build_dir
cd $build_dir
cmd="cmake .. -DCMAKE_BUILD_TYPE=Debug $toolchain_def $CMAKE_WA_SDK_DIR   -DCMAKE_LIBRARY_OUTPUT_DIRECTORY=$build_dir/bin"
echo $cmd
$cmd
[ $? -eq 0 ] || exit $?
make
[ $? -eq 0 ] || exit $?


echo "Great! Build successful. plugin modules generated in folder:"
echo "   $build_dir/bin"


if [ "$pack" = "yes" ]; then
    [ -d pack ] && rm -r pack
    echo ""
    echo "start to pack the plugins..."
    cd $build_dir
    mkdir -p $build_dir/pack/staging
    mkdir -p $build_dir/pack/staging/host

    cp $script_path/pack/package.info ./pack/staging
    cp -r $script_path/pack/ams pack/staging/host
    cp -r $script_path/pack/plugin.manifest pack/staging/host
    cp $build_dir/bin/*so pack/staging/host

    manifest_version=`cat pack/staging/host/plugin.manifest |grep "version" | head -n 1`
    echo "manifest_version=$manifest_version"

    pack_version=$(echo "$manifest_version" | awk -F '"' '{print $4}')
    echo "pack_version=$pack_version"
    pattern="^[0-9]+\.[0-9]+\.[0-9]+$"

    if [[ $pack_version =~ $pattern ]]; then
        echo "Version is valid."
    else
        echo "Version got from plugin.manifest is invalid."
        exit 1
    fi

    package_file_name=$build_dir/pack/plugin_hello_$pack_version
    [ -f $package_file_name ] && rm $package_file_name

    echo "packing from the folder: [$build_dir/pack/staging]"
    tree  $build_dir/pack/staging

    cmd="python3  $WA_SDK_DIR/tools/wa_pack.py -f $build_dir/pack/staging  -o $package_file_name   -v $pack_version"
    echo "cmd=$cmd"
    $cmd

    if [ -f $package_file_name.zip ]; then
        echo "Successful to generate $package_file_name"
    else
        echo "pack failed"
    fi      
else
    echo ""
    echo "If you want to pack it for installation, run command:"
    echo "  ./build_hello.sh -a"
fi

