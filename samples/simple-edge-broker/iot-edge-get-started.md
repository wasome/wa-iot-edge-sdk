
## Install the prerequisites

The steps in this tutorial assume you are running Ubuntu Linux.

To install the prerequisite packages, open a shell and run the following commands:

```bash
sudo apt-get update
sudo apt-get install curl build-essential libcurl4-openssl-dev git cmake pkg-config libssl-dev uuid-dev valgrind libglib2.0-dev libtool autoconf
```


## How to build the sample

You can now build the IoT Edge runtime and samples on your local machine:

1. Open a shell.

1. Navigate to the root folder in your local copy of the **iagent-sdk** repository.

1. Run the build script as follows:

    ```sh
    cd samples/simple-edge-broker/hello_world_main
    ./build_hello_main.sh
    ```


## How to run the sample

The **build.sh** script generates its output in the **v1/build** folder in your local copy of the **iot-edge** repository. This output includes the two IoT Edge modules used in this sample.


The hello\_world\_sample process takes the path to a JSON configuration file as a command-line argument. The following example JSON file is provided in the SDK repository at **samples/hello\_world/src/hello\_world\_lin.json**. This configuration file works as is unless you modify the build script to place the IoT Edge modules or sample executables in non-default locations.

> [!NOTE]
> The module paths are relative to the current working directory from where the hello\_world\_sample executable is launched, not the directory where the executable is located. The sample JSON configuration file defaults to writing 'log.txt' in your current working directory.

```json
{
    "modules" :
    [
        {
            "name" : "logger",
            "loader": {
            "name": "native",
            "entrypoint": {
                "module.path": "./modules/logger/liblogger.so"
            }
            },
            "args" : {"filename":"log.txt"}
        },
        {
            "name" : "hello_world",
            "loader": {
            "name": "native",
            "entrypoint": {
                "module.path": "./modules/hello_world/libhello_world.so"
            }
            },
            "args" : null
        }
    ],
    "links":
    [
        {
            "source": "hello_world",
            "sink": "logger"
        }
    ]
}
```

1. Navigate to the **./build/bin** folder.

2. Run the following command:

    ```sh
    cd samples/simple-edge-broker/hello_world_main/build/bin
    ./hello_world_sample ./hello_world_lin.json
    ```

## Typical output

The following example shows the output written to the log file by the Hello World sample. The output is formatted for legibility:

```json

```

## Code snippets

This section discusses some key sections of the code in the hello\_world sample.

### IoT Edge gateway creation

To create a gateway, implement a *gateway process*. This program creates the internal infrastructure (the broker), loads the IoT Edge modules, and configures the gateway process. IoT Edge provides the **Gateway\_Create\_From\_JSON** function to enable you to bootstrap a gateway from a JSON file. To use the **Gateway\_Create\_From\_JSON** function, pass it the path to a JSON file that specifies the IoT Edge modules to load.

You can find the code for the gateway process in the *Hello World* sample in the [main.c][lnk-main-c] file. For legibility, the following snippet shows an abbreviated version of the gateway process code. This example program creates a gateway and then waits for the user to press the **ENTER** key before it tears down the gateway.

```C
int main(int argc, char** argv)
{
    GATEWAY_HANDLE gateway;
    if ((gateway = Gateway_Create_From_JSON(argv[1])) == NULL)
    {
        printf("failed to create the gateway from JSON\n");
    }
    else
    {
        printf("gateway successfully created from JSON\n");
        printf("gateway shall run until ENTER is pressed\n");
        (void)getchar();
        Gateway_LL_Destroy(gateway);
    }
    return 0;
}
```

The JSON settings file contains a list of IoT Edge modules to load and the links between the modules. Each IoT Edge module must specify a:

* **name**: a unique name for the module.
* **loader**: a loader that knows how to load the desired module. 
    * **name**: the name of the loader used to load the module.
    * **entrypoint**: the path to the library containing the module. On Linux, this library is a .so file, on Windows this library is a .dll file. The entry point is specific to the type of loader being used.

* **args**: any configuration information the module needs.

The following code shows the JSON used to declare all the IoT Edge modules for the Hello World sample on Linux. Whether a module requires any arguments depends on the design of the module. In this example, the client module takes an argument that is the path to the output file and the hello\_world module has no arguments.

```json
{
  "modules": [
    {
      "name": "client",
      "loader": {
        "name": "native",
        "entrypoint": {
          "module.path": "./libhello_world_client.so"
        }
      },
      "args": {
        "filename": "log.txt"
      }
    },
    {
      "name": "server",
      "loader": {
        "name": "native",
        "entrypoint": {
          "module.path": "./libhello_world_service.so"
        }
      },
      "args": {
        
      }
    }
  ],
  "links": [
  ]
}```

The JSON file also contains the links between the modules that are passed to the broker. A link has two properties:

* **source**: a module name from the `modules` section, or `\*`.
* **sink**: a module name from the `modules` section.

Each link defines a message route and direction. Messages from the **source** module are delivered to the **sink** module. You can set the **source** module to `\*`, which indicates that the **sink** module receives messages from any module.

The following code shows the JSON used to configure links between the modules used in the hello\_world sample on Linux. Every message produced by the `client` module is consumed by the `server` module.

```json
"links":
[
    {
        "source": "client",
        "sink": "server"
    }
]
```

### Hello\_world module message publishing

You can find the code used by the hello\_world module to publish messages in the ['hello_world.c'][lnk-helloworld-c] file. The following snippet shows an amended version of the code with comments added and some error handling code removed for legibility:

```C
int helloWorldThread(void *param)
{
    // create data structures used in function.
    HELLOWORLD_HANDLE_DATA* handleData = param;
    MESSAGE_CONFIG msgConfig;
    MAP_HANDLE propertiesMap = Map_Create(NULL);

    // add a property named "helloWorld" with a value of "from Azure IoT
    // Gateway SDK simple sample!" to a set of message properties that
    // will be appended to the message before publishing it. 
    Map_AddOrUpdate(propertiesMap, "helloWorld", "from Azure IoT Gateway SDK simple sample!")

    // set the content for the message
    msgConfig.size = strlen(HELLOWORLD_MESSAGE);
    msgConfig.source = HELLOWORLD_MESSAGE;

    // set the properties for the message
    msgConfig.sourceProperties = propertiesMap;

    // create a message based on the msgConfig structure
    MESSAGE_HANDLE helloWorldMessage = Message_Create(&msgConfig);

    while (1)
    {
        if (handleData->stopThread)
        {
            (void)Unlock(handleData->lockHandle);
            break; /*gets out of the thread*/
        }
        else
        {
            // publish the message to the broker
            (void)Broker_Publish(handleData->brokerHandle, helloWorldMessage);
            (void)Unlock(handleData->lockHandle);
        }

        (void)ThreadAPI_Sleep(5000); /*every 5 seconds*/
    }

    Message_Destroy(helloWorldMessage);

    return 0;
}
```

The hello\_world module never processes messages that other IoT Edge modules publish to the broker. Therefore, the implementation of the message callback in the hello\_world module is a no-op function.

```C
static void HelloWorld_Receive(MODULE_HANDLE moduleHandle, MESSAGE_HANDLE messageHandle)
{
    /* No action, HelloWorld is not interested in any messages. */
}
```



<!-- Links -->
