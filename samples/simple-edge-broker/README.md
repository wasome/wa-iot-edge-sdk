# 插件Hello world开发示例

注意：需要在本机X86 Ubuntu18.04以上环境编译和执行本程序。

## 快速上手
### 编译
运行：
```
./build_broker.sh 
```

### 运行

运行：
```
./build_broker.sh run
```

如果想打开helloworld插件的WA_THREAD_DEMO宏定义，执行：
```
WA_THREAD_DEMO=true ./build_broker.sh run
```


## Linux 间接引用 .so 的路径问题

执行上面的命令如果遇到以下错误：
```
./hello_world_sample: error while loading shared libraries: libnanomsg.so.5.0.0: cannot open shared object file: No such file or directory
```
这是因为无法定位被libgateway.so间接引用的libnanomsg.so文件：
```
oem@web01:~/repos/wa-iot-edge-sdk/samples/simple-edge-broker/build/bin$ ldd hello_world_sample
        linux-vdso.so.1 (0x00007ffc4718d000)
        libgateway.so => /home/oem/repos/wa-iot-edge-sdk/samples/../wa-iot-sdk/host/lib/libgateway.so (0x00007f522e9b6000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f522e7b2000)
        libnanomsg.so.5.0.0 => not found
        libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f522e7ac000)
        libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f522e65b000)
        libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f522e638000)
        librt.so.1 => /lib/x86_64-linux-gnu/librt.so.1 (0x00007f522e62e000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f522ebeb000)
```

在生产环境中，所有框架提供的动态库文件都会被正确设置路径，不会出现此问题。

在开发环境下可以参考如下文章解决Linux 间接引用 .so 的路径问题：
https://www.cnblogs.com/oloroso/p/13224975.html

如：  
```
LD_LIBRARY_PATH=$WA_SDK_DIR/lib ./hello_world_sample hello_world_lin.json
```
