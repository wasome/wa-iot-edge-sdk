'''
Copyright (c) WanSheng Intelligent Corp. All rights reserved.
Licensed under the MIT license. See LICENSE file in the project root for full license information.
'''

import time
import logging
import wagent.framework
import wagent.model
from wa_edge_iot.framework.wagent import I_wagent
from wa_edge_iot.framework.rd_query_param import RDQueryParam
from wa_edge_iot.framework.data_process import DataProcess
from wa_edge_iot.model.rd_resource import RdResource
from coapthon import defines


if __name__ == '__main__':
    

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger = logging.getLogger('coapthon')
    logger.setLevel(logging.ERROR)
   

    agent = I_wagent("test01-app", redis_port = None)
    print ('query all alarms..')
    alarms = I_wagent().query_alarm(None, 'group', 'group-2', active_only=False)
    if alarms:
        alarms.pretty_print()
        print ('payload:' + str(alarms.payload))
    else:
        print('no response')

    print ('query all alarms status..')
    alarms = I_wagent().query_alarm(2, 'group', query_stats=True, active_only=False)
    if alarms:
        alarms.pretty_print()
        print ('payload:' + str(alarms.payload))
    else:
        print('no response')


    print ('query all alarms status..')
    alarms = I_wagent().query_alarm(2, 'group', query_stats=False, active_only=False)
    if alarms:
        alarms.pretty_print()
        print ('payload:' + str(alarms.payload))
    else:
        print('no response')


    cnt = 0
    while cnt < 10:
        cnt = cnt + 1
        try:
            a = int(round(time.time() * 1000))
            code = I_wagent().post_alarm(cnt, 'group', 'group-{}'.format(cnt%4), 'this a test', 'xxxx content', cnt%3, '111group')
            b = int(round(time.time() * 1000))
            print(("----->raise alarm: " + str(code) + ', elpased ms : '  + str(b - a)))
            
            if code != defines.Codes.CHANGED.number:
                print ("!!!!!!!!")
                
        except Exception as e:
                print((str(e) ))

    print ('query all alarms 2..')
    alarms = I_wagent().query_alarm(2, 'group', None)
    if alarms:
        alarms.pretty_print()
    else:
        print('no response')

    print ('query alarms 3..group-1')
    alarms = I_wagent().query_alarm(3, 'group', 'group-1')
    if alarms:
        alarms.pretty_print()
    else:
        print('no response')

    cnt = 0
    while cnt < 7:
        cnt = cnt + 1
        try:
            code = I_wagent().clear_alarm(cnt, 'group', 'group-{}'.format(cnt%4), "test clear")
            
            if code != defines.Codes.CHANGED.number:
                 print(("clear alarm: " + str(code)))
            
            time.sleep(1)
                
        except Exception as e:
                print((str(e) ))

    print ('query alarms..all cleared some')
    alarms = I_wagent().query_alarm('test_id-1', 'group')
    if alarms:
        alarms.pretty_print()    