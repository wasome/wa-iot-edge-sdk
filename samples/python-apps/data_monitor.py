'''
Copyright (c) WanSheng Intelligent Corp. All rights reserved.
Licensed under the MIT license. See LICENSE file in the project root for full license information.
'''

import time
import logging
import coapthon
import wagent.framework
import wagent.model
from wa_edge_iot.framework import wagent
from wa_edge_iot.framework.rd_query_param import RDQueryParam
from wa_edge_iot.framework.handler_base import CoAPHandlersBase
from wa_edge_iot.framework.data_process import DataProcess
from wa_edge_iot.model.rd_resource import RdResource
from wa_edge_iot.model.resource_data_base import ResourceDataBase

class MyHandlers(CoAPHandlersBase):

    def __init__(self):
        '''
        Constructor
        '''
    
    # device change of monitored devices
    def on_monitored_device_change(self, devices, monitor_name):
        print("on_monitored_device_change:  monitor_name={}".format(monitor_name))
        for device in devices:
            assert isinstance(device, DeviceInfo)
            device.dump()
        
        return
    
    def on_resource_data_process(self, di, ri, data, monitor_name, data_process):
        print("on_resource_data_process:  monitor_name={}, di={}, ri={}".format(monitor_name, di, ri))
        
        assert isinstance(data, ResourceDataBase)
        assert data_process is None or isinstance(data_process, DataProcess)
        if data_process is None:
            print("notify only.. don't process")
            return
        
        if monitor_name == "hero":
            print("decided to continue  DATA")
            data_process.set_decision(DataProcess.CONTINUE)
        if monitor_name == "hello-1":
            data_process.set_decision(DataProcess.CONTINUE)
            print("decided to continue CURRENT DATA")
        if monitor_name == "haha":
            data_process.set_decision(DataProcess.DROP)
            print("decided to DROP CURRENT DATA")
                    
        return
    
    


if __name__ == '__main__':
    logger = logging.getLogger('coapthon')
    logger.setLevel(logging.ERROR)
    
    my_handlers = MyHandlers()
    wagent = wagent.I_wagent("test01-app", 
                                          CoAP_handlers=my_handlers, 
                                          redis_port = None)

    

    di = wagent.wagent_id() + "-m"
    rd_query = RDQueryParam(standard_type='lwm2m');
    devices = wagent.do_device_query()
    
    print("return devices: "+ str(len(devices)))
    if len(devices) > 0:
        di = devices[0].get_device_id()
        print('set di to ' + di)
        
    res = devices[0].get_resources()[0]
    assert isinstance(res, RdResource)
    
    
    
    monitors = []    
    id = res.enable_monitoring(res.get_href(), None)
    print("resource monitor: " + str(id))
    monitors.append(id)

    
    id = wagent.add_data_monitor_point(di, "/1/0",  "1", "hello-1", process=True)
    print("add moinitor point: " + str(id))
    monitors.append(id)

    id = wagent.add_data_monitor_point(di, "/1/0/1",  None, "hello-2", process=False, sequence=2)
    print("add moinitor point: " + str(id))
    monitors.append(id)

    
    id = wagent.add_data_monitor_point(di, "/1/0/1", None, "hello-3", process=True, sequence=3)
    print("add moinitor point: " + str(id))
    monitors.append(id)
    
    try:
            time.sleep(1000)
    except KeyboardInterrupt:        
        print("user kill the process..")

    for id in monitors:
            wagent.remove_data_monitor_point(id)

    wagent.stop_monitoring()

    print("exit")
    