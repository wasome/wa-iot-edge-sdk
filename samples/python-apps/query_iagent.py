'''
Copyright (c) WanSheng Intelligent Corp. All rights reserved.
Licensed under the MIT license. See LICENSE file in the project root for full license information.
'''

import time
import logging
import coapthon
import wagent.framework
import wagent.model
from wa_edge_iot.framework import wagent
from wa_edge_iot.framework.rd_query_param import RDQueryParam
from wa_edge_iot.framework.data_process import DataProcess
from wa_edge_iot.model.rd_resource import RdResource


if __name__ == '__main__':
    

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger = logging.getLogger('coapthon')
    logger.setLevel(logging.ERROR)
    
    print("all logs:")
    for log in logging.Logger.manager.loggerDict:
        logger = logging.getLogger(log)
        print(log+ ", getEffectiveLevel: " + str(logger.getEffectiveLevel()))

    agent = wagent.I_wagent("test01-app", redis_port = None)
    
    data = agent.data_util().get("ilink")
    print(str(data))
    
    iagent_info = agent.get_iagent_info()
    if iagent_info:
        iagent_info.dump()
        

    if iagent_info.wagent_id() == "No ID":
        di = "lwm2mdaemon-m"
    else:
        di = iagent_info.wagent_id() + "-m"
    rd_query = RDQueryParam(di);
    devices = agent.do_device_query(rd_query)
    print("return devices: "+ str(len(devices)))
    device = devices[0]
    device.dump()
    
    resources = device.get_resources()
    res = device.find_resource('/10241/0');
    assert isinstance(res, RdResource)
    data = res.get();
    if (data) :
        fmt, payload = data.pack()
        print(str(payload))

    res.put(0, '1', '1')   
    print ("put : 10") 
    res.put(0, '1', '10')    

    print("exit")
    