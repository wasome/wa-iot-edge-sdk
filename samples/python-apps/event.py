'''
Copyright (c) WanSheng Intelligent Corp. All rights reserved.
Licensed under the MIT license. See LICENSE file in the project root for full license information.
'''

import time
import logging
import wagent.model
from wa_edge_iot.framework.wagent import I_wagent
from wa_edge_iot.framework.rd_query_param import RDQueryParam
from wa_edge_iot.framework.data_process import DataProcess
from wa_edge_iot.model.rd_resource import RdResource
from coapthon import defines


if __name__ == '__main__':
    

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger = logging.getLogger('coapthon')
    logger.setLevel(logging.ERROR)
   

    agent = I_wagent("test02-app", redis_port = None)
    print ('query all events..')
    alarms = I_wagent().query_event(None, 'group', 'group-2')
    if alarms:
        alarms.pretty_print()
        print ('payload:' + str(alarms.payload))
    else:
        print('no response')

    cnt = 0
    while cnt < 10:
        cnt = cnt + 1
        try:
            a = int(round(time.time() * 1000))
            code = I_wagent().post_event(cnt, 'group', 'group-{}'.format(cnt%4), 'this a test', 'xxxx content', cnt%3)
            b = int(round(time.time() * 1000))
            print(("----->raise event: " + str(code) + ', elpased ms : '  + str(b - a)))
            
            if code != defines.Codes.CHANGED.number:
                print ("!!!!!!!!")

            print ('query  events {}..group-{}'.format(cnt, cnt%4))
            events = I_wagent().query_event(cnt, 'group', 'group-{}'.format(cnt%4))
            if events:
                print ('payload:' + str(events.payload))
            else:
                print('no response')
        except Exception as e:
                print((str(e) ))

    print ('query event all under group')
    alarms = I_wagent().query_event(None, 'group', None)
    if alarms:
        alarms.pretty_print()
    else:
        print('no response')

    print ('query alarms..all ')
    alarms = I_wagent().query_event(None, None, None)
    if alarms:
        alarms.pretty_print()    