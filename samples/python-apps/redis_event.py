'''
Copyright (c) WanSheng Intelligent Corp. All rights reserved.
Licensed under the MIT license. See LICENSE file in the project root for full license information.
'''

import time
import logging
import coapthon
import wa_edge_iot.framework
import wa_edge_iot.model
from wa_edge_iot.framework import wagent
from wa_edge_iot.framework.rd_query_param import RDQueryParam
from wa_edge_iot.framework.handler_base import CacheDbHandlersBase
from wa_edge_iot.framework.data_process import DataProcess
from wa_edge_iot.model.rd_resource import RdResource
from wa_edge_iot.model.resource_data_base import ResourceDataBase
from wa_edge_iot.model.property_data import PropertyData

class MyHandlers(CacheDbHandlersBase):

    def __init__(self):
        '''
        Constructor
        '''
    # callback for any device registration and de-registration
    def on_any_device_registration(self, di, status):    
        print("on_any_device_registration: di={}, status={}".format(di, status))
        deviceObj = wagent.redis_util().get_device(di)
        if deviceObj:
            deviceObj.dump()
        else:
            print("on_cached_data_updated: no device returned form redis")
                    
        return
    
    
    def on_cached_data_updated(self, di, ri, property, value):
               
        print("on_cached_data_updated: di={}, ri={}, property={}, value={}".format(di, ri, property, value))
        if property:
            read_value = wagent.redis_util().get_property_value(di, ri, property)
            if read_value:
                assert isinstance(read_value, PropertyData)
                print("read value from cacheDB: " + read_value.describe())
        else:
            res = wagent.redis_util().get_resource_data(di, ri)
            assert isinstance(res, ResourceDataBase)
            print(res.pack())
            
        return
    
    def on_system_event(self, event_type, event_msg):
        print("on_system_event: "+ str(event_msg))
        return
    
device_id="mb2_1"
resource_id="/mmmm"

if __name__ == '__main__':
    my_handlers = MyHandlers()
    wagent = wagent.I_wagent("test01-app", 
            CacheDb_handlers=my_handlers)


    edge_di = wagent.wagent_id();
    if edge_di is None:
        print("no wagent id returned")
        exit()
    print("edge id: " + edge_di)
    
    res = None
    deviceObj = wagent.redis_util().get_device(device_id)
    if deviceObj:
        deviceObj.dump()
        res = deviceObj.find_resource(resource_id)
        
    else:
        print("no device returned form redis")

    
    if not res:
        print("no resource returned form redis")
        exit()

    assert isinstance(res, RdResource)
       
    monitors = []    
    id = res.enable_monitoring(None, None, interval=3)
    print("resource monitor: " + str(id))
    monitors.append(id)
    
    try:
        cnt = 1
        while cnt < 1000:
            cnt += 1
            time.sleep(1)
    except KeyboardInterrupt:        
        print("user kill the process..")

    for id in monitors:
            wagent.remove_data_monitor_point(id)

    wagent.stop_monitoring()

    print("exit")
    