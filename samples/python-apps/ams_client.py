'''
Copyright (c) WanSheng Intelligent Corp. All rights reserved.
Licensed under the MIT license. See LICENSE file in the project root for full license information.
'''


import time
import logging
import coapthon
from wa_edge_iot.ams.ams_user import AmsUser 




if __name__ == '__main__':
    amsuser = AmsUser("tester")

    config_path = amsuser.get_config_file_path("test.cfg", "device", "432141324")
    print ("config path:" + config_path)

    if amsuser.set_product_id("my_id"):
        print ("set_product_id pass")
    else:
        print ("set_product_id failed")


    if amsuser.add_checkpoint("device", "12345"):
        print ("add_checkpoint pass")
    else:
        print ("add_checkpoint failed")

    if amsuser.delete_checkpoint("device", "12345"):
        print ("delete_checkpoint pass")
    else:
        print ("delete_checkpoint failed")

