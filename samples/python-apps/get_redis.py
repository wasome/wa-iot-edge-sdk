'''
Copyright (c) WanSheng Intelligent Corp. All rights reserved.
Licensed under the MIT license. See LICENSE file in the project root for full license information.
'''

import time
import logging
import coapthon
import wagent.framework
import wagent.model
from wa_edge_iot.framework import wagent
from wa_edge_iot.framework.rd_query_param import RDQueryParam
from wa_edge_iot.model.rd_resource import RdResource
from wa_edge_iot.model.resource_data_base import ResourceDataBase

if __name__ == '__main__':
    agent = wagent.I_wagent("test01-app")
    di = agent.wagent_id() + "-m"
    
    deviceObj = agent.redis_util().get_device(di)
    if deviceObj:
        deviceObj.dump()
    else:
        print("no device returned form redis")
    
    data = agent.redis_util().get_resource_data(di, '/1/0')
    if data:
        print(data.pack()[1])

    data = agent.redis_util().get_property_value(di, '/1/0', '1')
    if data:
        print('property value:' + str(data.value))
    
    rd_query = RDQueryParam(di);
    devices = agent.do_device_query()
    print("return devices: "+ str(len(devices)))

    res = devices[0].get_resources()[0]
    assert isinstance(res, RdResource)
    
    print("exit")
    