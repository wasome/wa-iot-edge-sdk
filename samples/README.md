# 示例程序

## clients

包含使用WA-SDK开发的若干示例程序，以独立的进程方式运行，和WA-IOT-EDGE平台进行交互。

### ble-reader

本例子展示读取蓝牙传感器数据，并上报给WA-IOT-EDGE平台。本例子中设备是`passive`模式，不接收来自外部的请求，仅仅在数据就绪时主动发出数据。这种模式多用于低功耗设备，平时处于睡眠状态。

[进入>>>](./clients/ble-reader)


### endpoint

本例子展示一个基于CoAP接口的标准设备。
1. 该设备支持上报资源目录(RD)消息，来通知本设备所支持的资源信息，
2. 使用RD ttl参数来定期刷新保持在线状态。
3. 支持外部请求读或者写本设备上的资源
4. 在WA-IOT-EDGE平台管理上对本设备进行管理和配置

[进入>>>](./clients/endpoint)

### electric-power

这个例子里，通过在WebConsole配置，Wagent已经会自动从modbus上读取电表里的数据。 

report进程通过wAgent接口监听所有的读取的电表数据，然后基于电力104规约上报到云端（上报功能未实现）  

同时也展示如何交叉编译，并打包到ams安装包，从云上下发到边缘平台上。

[进入>>>](./clients/electric-power)


### data-flow

本例子展示基于CoAP接口参与WA-IOT-EDGE所有设备数据流的处理
 
[进入>>>](./clients/data-flow)

## plugin-hello

用于快速上手的插件开发示例，包含分别服务侧和客户端侧的插件，分别提供服务和访问服务。

[进入>>>](./plugin-hello)

## python-apps

基于python API实现的若干访问WA-IOT-EDGE平台的client程序

注意：当前尚未完全可用

[进入>>>](./python-apps)

## simple-edge-broker

一个简单的edge-broker程序，可以不使用WA-IOT-EDGE平台，用来测试加载和运行所开发的插件。

上面的命令将分别使用本机和ARM 编译器进行编译，并打包。 可在WebConsole上安装与管理软件包。