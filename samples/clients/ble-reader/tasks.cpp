
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#define LOG_TAG "ble"

#include "ble_reader.h"
#include <list>
#include <string>

wa_task_scheduler_t g_task_ctx = NULL;
int g_frequency = 10;
std::list<std::string> ble_mac_list;

void scan_ble(std::list<std::string> & ble_mac_list)
{
    // simulating scaning ble devices
    ble_mac_list.push_back("ble0001");
    ble_mac_list.push_back("ble0002");
}

int read_ble(const char *ble_mac)
{
    // simulating the data read from ble sensor
    return rand();
}


// it will be called periodically
int task_read_sensor(wa_task_t task)
{
    
    std::list<std::string>::iterator it;
    TraceI(FLAG_DEFAULT, "task_read_sensor: ENTER");

    for(it = ble_mac_list.begin(); it != ble_mac_list.end(); it++)
    {
        const char *di = (*it).c_str();
        const char *ri = "/heartbeat";
        char payload[256];
        /// read the BLE device
        int data = read_ble((*it).c_str());

        snprintf(payload, sizeof(payload), "%d", data);

        /// Post the data to the iagent interface "PUT /dp/{device id}/{resource ID}"
        char uri[256];
        snprintf(uri, sizeof(uri), "/dp/%s%s", di, ri);
        restful_request_t request[1];
        wa_setup_request(request, T_Post, IA_TEXT_PLAIN, 
            strlen(payload), payload, 
            uri, 
            (char*)"pt=value");  // property name is "value"

        // simply post the DP message without waiting the response
        wa_coap_send_request(request, "127.0.0.1", 5683);
    }

    // the task can change the repeating frequence
    task->repeat_interval_msecs = g_frequency*1000;

    return 0;
}


int task_register_RD(wa_task_t task)
{
    std::list<std::string>::iterator it;
    for(it = ble_mac_list.begin(); it != ble_mac_list.end(); it++)
    {

        // prepare the resource directory report message
        rd_assemble_t rd = rd_data_init_A(it->c_str(), "ep", "wasome.ble", true);

        // Tell this device only post message, and never recieve and handle incoming request
        rd_data_set_passive(rd);

        // generate the message payload in JSON format
        char * RD_message = rd_data_payload_A(rd);
        WALOG("RD_message: %s", RD_message);

        rd_data_destroy(rd);

        restful_request_t request[1];
        wa_setup_request(request, T_Post, IA_APPLICATION_JSON, strlen(RD_message), RD_message, (char*)"rd", NULL);

        // simply post the RD message without waiting the response
        wa_coap_send_request(request, "127.0.0.1", 5683);
    }
}

// Show how to start task execution.
// Do nothing really useful here.

void init_task_thread()
{

    /// Scan BLE devices
    scan_ble(ble_mac_list);

    g_task_ctx = bh_task_run_new_scheduler();
    wa_task_t task1 =  bh_new_task((char*)ZS_SENSOR_READ_TASK,
            NULL,
            true,
            10*1000,   // repeat task every 10 seconds
            task_read_sensor);

    bh_schedule_task(g_task_ctx, task1, 1);

    wa_task_t task2 =  bh_new_task((char*)ZS_REG_RD_TASK,
            NULL,
            true,
            60*1000,   // repeat task every 60 seconds
            task_register_RD);

    bh_schedule_task(g_task_ctx, task2, 0);
   
}
