# BLE READER

本例子展示读取蓝牙传感器数据，并上报给WA-IOT-EDGE平台。本例子中设备是`passive`模式，不接收来自外部的请求，仅仅在数据就绪时主动发出数据。这种模式多用于低功耗设备，平时处于睡眠状态。

## 使用WA-IOT-SDK接口
1. 使用wa-iot-edge支持的**任务（task）**编程接口，启动两个定期任务，分别上报蓝牙设备的资源目录（RD）来进入在线状态和数据点(DP)
   - bh_task_run_new_scheduler
   - bh_new_task
   - bh_schedule_task

2. 使用wa-iot-edge支持的**资源目录（rd）**编程接口
    - rd_data_init_A
    - rd_data_set_passive
    - rd_data_payload_A
    - rd_data_destroy

3. 使用wa-iot-edge支持的**coap和restful**编程接口来发送数据
    - wa_setup_request
    - wa_coap_send_request


## BUILD

执行命令：  
```
./build.sh
```