
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#include "ble_reader.h"

WA_EDGE_CONTEXT wagent_ctx = NULL;


int main(int argc, char * argv[])
{
    log_setlevel(LOG_VERBOSE);
    log_set_tag_mask(LOG_MY_MASK_ID, 0xFFFFFFFF);


    // start another thread for executing some tasks
    init_task_thread();

    // start the main loop for the restful framework handling all the messages
    while(1) 
    {
        sleep(10);
    };

    return 0;
}
