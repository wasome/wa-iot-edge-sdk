﻿# AMS打包

## package.info
这个文件在上传AMS包到云端后，AMS SERVER用来解析安装包用的。

* category: 不要修改此项内容
* cfg_id_list: 配置path_name和target_type的内容和代码中调用ams_add所输入的参数内容一致。
* product_name: 可以改成你的软件名称, 设置的内如必须和代码中ams_init所输入的参数一致。
* version: 安装包的版本号。如果打包脚本提供版本修改功能，不用每次更改此项