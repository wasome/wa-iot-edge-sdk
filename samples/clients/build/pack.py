#!/usr/bin/python3
import os
import sys
import time
import getopt
import logging
import json
from pprint import pprint
import shutil
import datetime
import argparse

global package_version
global lite_server_repo_dir
    
current_dir = ''
lite_server_repo_dir= ''


package_version = ''
def update_package_info(pack_info):
    print(pack_info)             
    info = json.loads(open(pack_info).read())
    info['version'] = package_version

    with open(pack_info, 'w') as file1:
        json.dump(info, file1, indent=4)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = "Build and pack ep project")
    parser.add_argument('-i', dest = 'increment', action = 'store_true',
            default = False, 
            help = 'incremental build. dont do clean build')
    
    args = parser.parse_args()

    current_dir = os.getcwd()
    build_dir_name = os.path.realpath(__file__)
    lite_server_repo_dir = "".join(build_dir_name.rsplit('/build/pack.py'))
   
    ams_pack_name = lite_server_repo_dir + '/../../scripts/ams/wa_pack.py'
    if not os.path.exists(ams_pack_name):
        print (f'pack tool [{ams_pack_name}] doesnt exist')
        exit(1)


    t = datetime.datetime.today()
    default_version = "{}-{}-{}".format(t.year, t.month,  t.day)
    
    package_version = input("[input package_version(" + default_version +"):]")
    if '' == package_version:
        package_version = default_version
   
    out_dir_name = lite_server_repo_dir + '/out'
    if False == os.path.exists(out_dir_name):
        os.makedirs(out_dir_name)
        
    ep_package_zip = out_dir_name + '/client_samples' + package_version+'.zip'
    print('output zip file located at :' + ep_package_zip)

    pack_dir_name = out_dir_name + '/pack'
    if True == os.path.exists(pack_dir_name):
        shutil.rmtree(pack_dir_name)
    os.makedirs(pack_dir_name)
    
    platforms = ["arm-a7", "host"]
    for p_option in  platforms:
        print("\n\n start build platform :" + p_option)
        pack_dir = pack_dir_name + '/' + p_option

        # setup the folder for building
        build_out_dir = lite_server_repo_dir + '/out/product/'+ p_option
        if False == os.path.exists(build_out_dir):
            os.makedirs(build_out_dir)

        # run the build.sh to build the source files
        build_script_file_name = lite_server_repo_dir + '/build/build.sh ' + ' -p ' + p_option 
        if not args.increment: 
            build_script_file_name = build_script_file_name + ' -c'
        else: 
            print("use incremental build..")

        print("build cmd:")
        print(build_script_file_name)
        ret = os.system(build_script_file_name)
        if ret != 0:
            fail_info = 'build for ' + p_option + ' ' + 'failed'
            logging.error('build ' + p_option + ' '+  ' failed')
            print(fail_info)
            exit(1)
        logging.info('build ' + p_option + ' '+ ' succeed')

        ## prepare the staging folder for packing
        if True == os.path.exists(pack_dir):
            shutil.rmtree(pack_dir)
        os.makedirs(pack_dir)

        # setup package .info
        pack_info = lite_server_repo_dir + '/build/package.info'
        shutil.copy(pack_info ,
                        pack_dir_name+ '/package.info'  ) 
        update_package_info(pack_dir_name+ '/package.info')

        # setup ams 
        os.makedirs(pack_dir + '/ams')
        pack_dir_triggers = pack_dir + '/ams/triggers'
        triggers_path = lite_server_repo_dir + '/build/ams-triggers/'
        shutil.copytree(triggers_path, pack_dir_triggers)

        os.makedirs(pack_dir + '/ams/version')
        shutil.move(build_out_dir + '/platform.info',
                        pack_dir + '/ams/version/platform.info')

        # setup executables
        shutil.copy(build_out_dir+ '/ep_reporter', pack_dir+ '/ep_reporter'  )
        shutil.copy(build_out_dir+ '/endpoint', pack_dir+ '/endpoint'  )
        shutil.copy(build_out_dir+ '/ble_reader', pack_dir+ '/ble_reader'  )
        shutil.copy(build_out_dir+ '/dataflow', pack_dir+ '/dataflow'  )
        
        # setup other builtin configuration files
        shutil.copy(lite_server_repo_dir + '/build/logcfg_default.ini' ,
                        pack_dir + '/logcfg_default.ini')
            
        print("finished build for platform : " + p_option + "\n\n")
                            
    print("now start to pack the ep into AMS package..")
    
    if os.path.isfile(ep_package_zip):
        os.remove(ep_package_zip)
    
    pack_cmd = 'python3 ' + ams_pack_name + ' -f ' + pack_dir_name + ' -o ' + ep_package_zip
    print(pack_cmd)
    os.system(pack_cmd)
    
