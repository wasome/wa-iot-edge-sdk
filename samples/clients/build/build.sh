#!/bin/bash
path=`dirname $0`
cd $path/..
base=`pwd`
repo_root_dir=$base/../..
platforms_dir=$repo_root_dir/scripts/platforms
echo platforms_dir=[$platforms_dir]

echo current dir is $base
usage ()
{
    echo "build.sh [options]"
    echo " -h                                       print help infomation"
    echo " -c                                       clean build"
    echo " -p [host | ]                             set target toolchain"
    exit 1
}

while getopts "chp:" arg
do
    case $arg in
        p)
        platform=$OPTARG;;
        c)
        clean="true";;
        h)
        usage;;
        ?)
        echo $arg
        usage;;
    esac
done

# p-platform
if [ ! -n "$platform" ]; then
    platform="host"
    
elif [ "$platform" != "host" ]; then
    [ -f $platforms_dir/$platform/toolchain.cmake ] || echo "toolchain.cmake is not present under  $platforms_dir/$platform/" || exit 1
    toolchain_def="-DCMAKE_TOOLCHAIN_FILE=$platforms_dir/$platform/toolchain.cmake"
    setenv_file=$platforms_dir/$platform/setenv.sh
fi

WA_SDK_DIR=$repo_root_dir/out/$platform/wa-iot
[ -d $WA_SDK_DIR ] || WA_SDK_DIR=$repo_root_dir/wa-iot-sdk/$platform
if [ !-d $WA_SDK_DIR ]; then
    echo "Can find sdk path for WA_SDK_DIR"
    exit 1
fi
CMAKE_WA_SDK_DIR="-DWA_SDK_DIR=$WA_SDK_DIR"

out=$base/out/build/$platform
product_dir=$base/out/product/$platform

if [ "$clean" = "true" ]; then
    if [  -d "$out" ]; then
        rm -rf $out
        echo "clean build: removed folder "  $out
    fi
fi

mkdir -p $out
mkdir -p $product_dir

echo "Build platform(-p)        : [$platform]" 
echo "out put directory is(-o)  : [$out]" 
        
echo "~~~~~~~~~~~~~~~~~"
echo "Starting build..."

cd $out
	
echo ""
echo "now run cmake for project " 
echo "setenv_file       : [$setenv_file]" 
if [ ! -n "$setenv_file" ]; then
	echo "host needn't setenv"
else
	source $setenv_file
fi

cd $out/

cmd="cmake $toolchain_def $CMAKE_WA_SDK_DIR -DBINARY_DIR=$product_dir $base"
echo $cmd
$cmd
[ $? -eq 0 ] || exit $?
make
[ $? -eq 0 ] || exit $?

cp $platforms_dir/$platform/platform.info $base/out/product/$platform

cd $out
pwd
ls

echo "\033[32m <<<<<<<<<<<<<<<<<<<<<<<<<<<build pass!\033[0m"

exit 0

