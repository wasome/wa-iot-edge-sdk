
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#include "dataflow.h"

WA_EDGE_CONTEXT wagent_ctx = NULL;
const char * my_app_id = "data_mon_sample";

int main(int argc, char * argv[])
{
    bool status;
    log_setlevel(LOG_VERBOSE);
    log_set_tag_mask(LOG_WAGENT_SDK_MASK_ID, 0xFFFF);
    log_set_tag_mask(LOG_MY_MASK_ID, 0xFFFFFFFF);

    // initiate the coap based restful framework
    RESTFUL_CONTEXT restful_context = wa_coap_init_context(-1, 1000, 0);

    // initiate the communication with wagent
    wagent_ctx = wa_wagent_init(restful_context, "127.0.0.1");

    WA_DATA_MONITOR global_mon = wa_monitor_global_new(wagent_ctx, my_app_id, "tag1", data_global_handler);
    wa_monitor_set_process(global_mon, true);
    status = wa_monitor_run(global_mon);
    WARNING2("Start global monitor %s", status?"OK":"FAIL");

    // monitor the resource from sample "endpoint"
    WA_DATA_MONITOR mon1 = wa_monitor_res_new(wagent_ctx, my_app_id, "tag2", "ep_001", "/frequence", data_mon1_handler);
    wa_monitor_set_interval_ms(mon1, 2000);
    wa_monitor_set_process(mon1, true);
    wa_monitor_set_sequence(mon1, 10);
    status = wa_monitor_run(mon1);
    WARNING2("Start monitor #1: %s", status?"OK":"FAIL");

    // start the main loop for the restful framework handling all the messages
    while(1) 
    {
        // check the expiry of transactions
        uint32_t near_task_time = wa_coap_check_expiry(restful_context);
        if(near_task_time == -1) near_task_time = 1000; 

        // doing blocking wait and ensured wakeup by the nearest expiration
        wa_coap_recieve_process(restful_context, (int)near_task_time);
    };

    return 0;
}
