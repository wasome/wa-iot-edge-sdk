# Data-Flow
本例子展示基于CoAP接口参与WA-IOT-EDGE所有设备数据流的处理。

WA-EDGE数据流引擎工作原理图：  
![](doc/data_process_flow_simple.png)  

**主要特点：**  
1) 解耦共同参与数据处理流程的多方应用  
2) 每个参与者可以决定一个数据的命运:  
   - 继续下一个流程节点处理  
   - 修改数据内容交给下一个节点处理  
   - 终止当前数据处理流程  



## 使用WA-IOT-SDK接口

1. 创建基于COAP的restful框架上下文, 以及创建与WA-IOT-EDGE平台通讯的上下文：
   - wa_coap_init_context
   - wa_wagent_init
2. 数据流处理：
   - wa_monitor_global_new
   - wa_monitor_res_new
   - wa_monitor_set_interval_ms
   - wa_monitor_set_process
   - wa_monitor_set_sequence
   - wa_monitor_run
3. restful框架的状态维护
   - wa_coap_check_expiry
   - wa_coap_recieve_process
4. 资源的实现中返回请求处理结果的函数
   - wa_set_response_status
   - wa_set_response

## BUILD

执行命令：  
```
./build.sh
```


## 代码结构

![](doc/code.png)