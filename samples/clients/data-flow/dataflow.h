
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#ifndef SDK_SAMPLE_APPS_BLE_READER_BLE_READER_H_
#define SDK_SAMPLE_APPS_BLE_READER_BLE_READER_H_

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stddef.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/stat.h>
#include <sys/statfs.h>
#include <errno.h>
#include <signal.h>
#include <dirent.h>
#include <sys/utsname.h>

#include "agent_core_lib.h"
#include "logs.h"
#include "ams_constants.h"
#include "wa_edge_client_api.h"
#include "wa_redis_api.h"
#include "path.h"
#include "er-coap-constants.h"
#include "coap_sdk.h"

#ifdef __cplusplus
extern "C" {
#endif

extern WA_EDGE_CONTEXT wagent_ctx;

void data_global_handler( restful_request_t * request, REQ_ENV_HANDLE);
void data_mon1_handler(restful_request_t * request, REQ_ENV_HANDLE);

#ifdef __cplusplus
}
#endif

#endif /* SDK_SAMPLE_APPS_BLE_READER_BLE_READER_H_ */
