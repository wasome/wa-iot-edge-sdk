# Endpoint
开发者除了使用插件创建OCF虚拟设备[(如modbus插件所介绍)](https://gitee.com/wasome/plugin_modbus/blob/master/README.md)，还可以开发OCF设备程序。

本例子展示一个基于CoAP接口的标准设备。
- OCF程序可以设备内置，也可以是设备代理
- OCF程序需实现两个主要场景：注册资源，处理资源请求
- 使用基于CoAP消息格式


![](doc/OCF_APP.png)


## 注册资源
1. 该设备支持上报资源目录(RD)消息，来通知本设备所支持的资源信息，
2. 使用RD ttl参数来定期刷新保持在线状态。

注册资源后，在WA-IOT-EDGE平台管理上对本设备进行管理和配置  
![](doc/endpoint_dev.png)



## 使用WA-IOT-SDK接口

1. 创建基于COAP的restful框架上下文, 以及创建与WA-IOT-EDGE平台通讯的上下文：
   - wa_coap_init_context
   - wa_wagent_init
2. 注册资源实现函数：
   - wa_coap_register_resource
   - wa_resource_set_type
3. 准备资源消息
   - rd_data_init_A
   - wa_load_resources_to_rd
   - rd_data_set_ttl
   - rd_data_payload_A
   - rd_data_destroy
4. 自动资源上报与周期更新
   - wa_wagent_set_rd
   - wa_agent_check_rd_registeration
5. restful框架的状态维护
   - wa_coap_check_expiry
   - wa_coap_recieve_process
6. 资源的实现中返回请求处理结果的函数
   - wa_set_response_status
   - wa_set_response

## BUILD

执行命令：  
```
./build.sh
```