
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#ifndef _ELECTRIC_POWER_METER__H_
#define _ELECTRIC_POWER_METER__H_

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stddef.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/stat.h>
#include <sys/statfs.h>
#include <errno.h>
#include <signal.h>
#include <dirent.h>
#include <sys/utsname.h>

#include "wa-core/agent_core_lib.h"
#include "wa-core/logs.h"
#include "wa-sdk/ams_constants.h"
#include "wa-sdk/wa_edge_client_api.h"
#include "wa-sdk/wa_redis_api.h"
#include "wa-sdk/path.h"
#include "wa-core/cJSON_ext.h"


#ifdef __cplusplus
extern "C" {
#endif


///
/// definition of LOG flags
///
#define FLAG_LOG_INIT   1
#define FLAG_UTIL       2
#define FLAG_VALUE      4


#define CONFIG_PATH_TEST "/ep_test.cfg"

extern char g_ep104_IP[128];
extern int g_ep104_port;

void cb_redis_notify(REDIS_EVENT_T event, char * subject, void * content, void * user_data);

void load_configs();

#ifdef __cplusplus
}
#endif

#endif /* ELECTRIC_POWER_METER__H_ */
