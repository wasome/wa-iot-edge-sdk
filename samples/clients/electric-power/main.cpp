
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#define LOG_TAG "ep"
#include "ep.h"

char g_ep104_IP[128] = {0};
int g_ep104_port = 0;

void load_configs()
{
    char config_path[PATH_LEN];
    cJSON * config_json = NULL;
    get_device_config_pathname((char *)CONFIG_PATH_TEST, config_path, sizeof(config_path));
    printf("config_path=%s\n", config_path);

    config_json = load_json_file_A((const char *)config_path);
    if(config_json == NULL)
        return;

    const char * IP = cJSONx_GetObjectString(config_json, "ep104_server_IP");
    int port = cJSONx_GetObjectNumber(config_json, "ep104_server_port", 9090);

    if(IP)
    {
        strncpy(g_ep104_IP, IP, sizeof(g_ep104_IP)-1);
        g_ep104_port = port;
        WALOG("config: IP [%s], port = %d", IP, port);
    }
    else
    {
        ERROR("Invalid IP in the config");
    }

    cJSON_Delete(config_json);
}


int main(int ac, char **av)
{
    char cfgname[256];
    printf("electric power reporter started\n");
    path_init(av[0]);

    // initialize the logging
    get_local_config_path(cfgname, sizeof(cfgname), (char *)"logcfg_default.ini");
    log_init("ep_reporter", get_log_path(), cfgname);

    // load the configuration downloaded from cloud
    load_configs();

    // start monitoring the data
    wa_redis_start_monitor(cb_redis_notify, (void*)NULL);

    while(1)
    {
        sleep(100);
    }

    return 0;
}
