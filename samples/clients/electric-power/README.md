# Electric power monitor project

## Project description

这个例子里，通过在WebConsole配置，Wagent已经会自动从modbus上读取电表里的数据。 

report进程通过wAgent接口监听所有的读取的电表数据，然后基于电力104规约上报到云端（上报功能未实现）  

同时也展示如何交叉编译，并打包到ams安装包，从云上下发到边缘平台上。

## 配置文件格式

配置文件ep_test.cfg包含104规约数据上报地址，文件格式：
{
    "ep104_server_IP": "192.168.1.111",
    "ep104_server_port": 9090
}


## development requirements

* Ubuntu 18.04 and abolve
* Run the script "prepare_dev.sh"under $repo-root/scripts/dev-setup folder

## build project

goto the folder build, and run command:

./build.sh
./build.sh -p [arm-a7]


## build and pack to AMS package

goto the folder build, and run command:
./pack.py

参考 [build/README.md](build/README.md) 关于AMS打包的更多细节。

## 