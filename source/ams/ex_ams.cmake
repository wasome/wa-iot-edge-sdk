set(EX_AMS_DIR ${CMAKE_CURRENT_LIST_DIR})

set(EX_AMS_SOURCE
    ${EX_AMS_DIR}/ams_config.c
) 

set(EX_PATH_SOURCE
    ${EX_AMS_DIR}/ams_path.c
    ${EX_AMS_DIR}/path.c
    ${EX_AMS_DIR}/ams_products.c
) 

include_directories(
    ${EX_AMS_DIR} 
)


set(EX_AMS_INSTALL_HEADERS
    ${EX_AMS_DIR}/ams_constants.h
    ${EX_AMS_DIR}/ams_path.h
    ${EX_AMS_DIR}/ams_products.h
    ${EX_AMS_DIR}/path.h
)