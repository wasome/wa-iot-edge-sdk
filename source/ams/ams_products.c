
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#define LOG_TAG "AMS"


#include "ams_products.h"
#include "cJSON_ext.h"
#include <string.h>

 cJSON* load_products_A(bool from_cloud)
{
    char path[PATH_LEN];
    get_ams_root(path, sizeof(path));
    if(!from_cloud)
        strncat(path, "ams_client/config/local_installation.DB", sizeof(path));
    else
        strncat(path, "ams_client/config/product_installation.DB", sizeof(path));

    cJSON * root = load_json_file_A(path);
    if(root == NULL) return NULL;

    if (cJSON_IsArray(root))
    {
        return root;
    }
    cJSON_Delete(root);
    return NULL;

}

cJSON* load_all_products_A()
{
    cJSON * products = load_products_A(true);
    cJSON * products2 = load_products_A(false);

    if(products == NULL && products2 == NULL)
        return NULL;

    else if(products2 == NULL)
        return products;

    else if(products == NULL)
        return products2;

    else 
    {
        cJSON * item;
        while((item = cJSON_DetachItemFromArray(products2, 0)) != NULL)
        {
            cJSON_AddItemToArray(products, item);
        }
        cJSON_Delete(products2);
        return products;
    }
}


bool check_runtime_installed(const char * runtime_name,const char * runtime_version)
{
    cJSON* products = load_all_products_A();
    if(products == NULL) 
        return NULL;

    cJSON * item;
    cJSON_ArrayForEach(item, products)
    {
        cJSON * category = cJSON_GetObjectItem(item, "category");
        cJSON * product = cJSON_GetObjectItem(item, "product");
        cJSON * version = cJSON_GetObjectItem(item, "version");
        if(!cJSON_IsString(category) || !cJSON_IsString(product) 
            || !cJSON_IsString(version))
            continue;

        if(strcasecmp(category->valuestring, "runtime_engine") != 0)
            continue;

        if(strcmp(product->valuestring, runtime_name) == 0 && 
            strcmp(version->valuestring, runtime_version) == 0)
        {
            cJSON_Delete(products);
            return true;
        }
    }
        
    cJSON_Delete(products);
    return false;
}



char* ams_load_product_property_A(const char * product,const char * version, ams_product_property_e property)
{
    char * ret = NULL;
    const char * key;
    if(property == Ams_Category)
        key = "category";
    else if(property == Ams_Subclass)
        key = "subclass";
    else if(property == Ams_Install_Path)
        key = "install_path";
    else if(property == Ams_Install_Src)
        key = "local";
    else
        return NULL;

    cJSON* products = load_all_products_A();
    if(products == NULL) 
        return NULL;

    cJSON * item;
    cJSON_ArrayForEach(item, products)
    {
        cJSON * jproduct = cJSON_GetObjectItem(item, "product");
        cJSON * jversion = cJSON_GetObjectItem(item, "version");
        if( !cJSON_IsString(jproduct) || !cJSON_IsString(jversion))
            continue;

        if(strcmp(jproduct->valuestring, product) == 0 && 
            strcmp(jversion->valuestring, version) == 0)
        {
            cJSON * jsonProperty = cJSON_GetObjectItem(item, key);
            if(!cJSON_IsString(jsonProperty))
            {
                continue;
            }

            ret = strdup(jsonProperty->valuestring);
            break;
        }
    }
        
    cJSON_Delete(products);
    return ret;
}


ams_category_e ams_get_product_category(const char* category)
{
    if (category == NULL)
        return ms_Category_Unknown;
        
    if(strcmp(category, "managed_app") == 0)
        return Ams_Category_Managed_App;

    if(strcmp(category, "runtime_engine") == 0)
        return Ams_Category_Runtime;

    if(strcmp(category, "docker") == 0)
        return Ams_Category_Docker;

    if(strcmp(category, "software_product") == 0)
        return Ams_Category_SW;

    return ms_Category_Unknown;

}
