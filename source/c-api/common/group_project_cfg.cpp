
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#define LOG_TAG "UTIL"

#include "wa_edge_configs.h"
#include "path.h"
#define PATH_LEN 256


char *api_get_project_cfg_path(char *buffer, int buffer_len, char *projectId)
{
    char path[PATH_LEN];

    api_get_agent_dir(path, sizeof(path));
    strcat(path, "config/project/");
    if(access(path, 0) == 0)
    {
        if(strlen(path) >= (size_t)buffer_len) return NULL;

        strcpy(buffer, path);
        strcat(buffer, projectId);
        strcat(buffer, "/project.cfg");
        return buffer;
    }
    else
    {
        printf("api_get_project_cfg_path: %s dir is not present\n", path);
    }

    return NULL;
}

JSON_Value *api_load_group_cfg_A(char *group)
{
    char path[PATH_LEN];
    JSON_Value *ams_cfg_val;

    if(api_get_group_cfg_path(path, sizeof(path), group, (char*)"group.cfg"))
    {
        ams_cfg_val = json_parse_file(path);
        return ams_cfg_val;
    }
    else
    {
        return NULL;
    }
}



/**********************************************************************************/
/*
 * get group.cfg cs value by key.
 * return deep copy "key" JSON value.
 *
 */
JSON_Value *api_get_group_cs_value(JSON_Value *group_cfg_root, char *key)
{
    JSON_Object *cs_obj = json_value_get_object(group_cfg_root);
    char *value_str;
    int i;

    if (JSONArray != json_value_get_type(json_object_get_value(cs_obj, "cs")))
    {
        ERROR("JSON type error: hope array at group.cfg->cs");
        return NULL;
    }
    JSON_Array *cs = json_object_get_array(cs_obj, "cs");
    for (i = 0; (size_t)i < json_array_get_count(cs); i++)
    {
        if (JSONObject != json_value_get_type(json_array_get_value(cs, i)))
        {
            ERROR("JSON type error: hope object at group.cfg->cs->%d", i);
            continue;
        }
        JSON_Object *cs_obj = json_array_get_object(cs, i);

        if (JSONString != json_value_get_type(json_object_get_value(cs_obj, "n")))
        {
            ERROR("JSON type error: hope string at group.cfg->cs->%d->n", i);
            continue;
        }

        if (strcmp(key, json_object_get_string(cs_obj, "n")) == 0)
        {
            value_str = (char*)json_object_get_string(cs_obj, "d");
            return json_parse_string(value_str);
        }
    }
    return NULL;
}

/**********************************************************************************/
/*
 * get group.cfg cs value by key.
 * return deep copy "key" JSON value.
 *
 */
JSON_Value *api_get_group_dss_value_A(JSON_Value *group_cfg_root, char *key)
{
    JSON_Object *cfg_obj = json_value_get_object(group_cfg_root);
    JSON_Array *dss_arr;
    JSON_Value *dev_val = NULL;
    JSON_Object *dev_obj;
    size_t i = 0;

    if (cfg_obj == NULL) return NULL;

    if (JSONArray != json_value_get_type(json_object_get_value(cfg_obj, "dss")))
    {
        ERROR("JSON type error: hope array at group.cfg->dss");
        return NULL;
    }
    dss_arr = json_array(json_object_get_value(cfg_obj, "dss"));

    for (i = 0; i < json_array_get_count(dss_arr); i++)
    {
        dev_val = json_array_get_value(dss_arr, i);
        if (JSONObject != json_value_get_type(dev_val))
        {
            ERROR("JSON type error: hope object at group.cfg->dss->%d", i);
            continue;
        }
        dev_obj = json_object(dev_val);

        if (JSONString != json_value_get_type(json_object_get_value(dev_obj, "n")))
        {
            ERROR("JSON type error: hope string at group.cfg->dss->%d->n", i);
            continue;
        }
        if (strcmp(key, json_object_get_string(dev_obj, "n")) == 0)
        {
            return json_value_deep_copy(dev_val);
        }
    }
    return NULL;
}




bool api_get_dss_resource_url(char *group, char *dss,  char * url_buffer, int buffer_len)
{
    JSON_Value *sensor_val;
    JSON_Object *sensor_obj;
    JSON_Object *data_obj;
    bool rst = false;
    char *dev = NULL;
    char *url = NULL;
    char *property = NULL;
    JSON_Value *group_val = NULL;

    group_val = api_load_group_cfg_A(group);
    if (group_val == NULL) return false;

    sensor_val = api_get_group_dss_value_A(group_val, dss);

    if (JSONObject != json_value_get_type(sensor_val))
    {
        ERROR("api_get_dss_resource_url:GROUP [%s] DSS [%s] is not JSON OBJECT",
                group, dss);
        goto END;
    }
    sensor_obj = json_value_get_object(sensor_val);

    if (JSONObject != json_value_get_type(json_object_get_value(sensor_obj, "d")))
    {
        ERROR("api_get_dss_resource_url:GROUP [%s] DSS [%s] has no field 'd'",
                group, dss);
        goto END;
    }
    data_obj = json_object(json_object_get_value(sensor_obj, "d"));

    dev = (char*)json_object_get_string(data_obj, "d");
    url = (char*)json_object_get_string(data_obj, "url");
    property = (char*)json_object_get_string(data_obj, "pn");

    if ((dev == NULL || dev[0] == '\0') ||
            (url == NULL || url[0] == '\0') ||
            (property == NULL || property[0] == '\0'))
    {
        ERROR("api_get_dss_resource_url:GROUP [%s] DSS [%s] not valid'",
                group, dss);
        goto END;
    }

    snprintf(url_buffer, buffer_len, "/dev/%s%s/%s", dev, url, property);
    rst = true;

END:
    json_value_free(group_val);
    if(sensor_val) json_value_free(sensor_val);
    return rst;
}


// return true if any display name changed for the room
char * api_get_group_display_name(char *group, char * buffer, int buffer_len)
{
    JSON_Value *group_val = NULL;
    JSON_Object *group_obj = NULL;
    char *dpn;

    group_val = api_load_group_cfg_A(group);
    if (group_val == NULL) return NULL;

    group_obj = json_value_get_object(group_val);

    dpn = (char*)json_object_get_string(group_obj, "dpn");
    if (dpn && dpn[0] != '\0')
        strncpy(buffer, dpn, buffer_len);
    else
        strncpy(buffer, group, buffer_len);

    if (group_val) json_value_free(group_val);
    return buffer;
}
