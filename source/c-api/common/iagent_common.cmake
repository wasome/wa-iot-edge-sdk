set(IAGENT_COMMON_DIR ${CMAKE_CURRENT_LIST_DIR})

include_directories(${IAGENT_COMMON_DIR})


set(IAGENT_COMMON_SOURCE
    ${IAGENT_COMMON_DIR}/alarm_parser.c
    ${IAGENT_COMMON_DIR}/event_parser.c
    ${IAGENT_COMMON_DIR}/payload_rd.cpp
    ${IAGENT_COMMON_DIR}/wa_edge_configs.cpp
    ${IAGENT_COMMON_DIR}/group_project_cfg.cpp
)

set(AGENT_COMMON_INSTALL_HEADERS
    ${IAGENT_COMMON_DIR}/wa_edge_types.h
    ${IAGENT_COMMON_DIR}/wa_edge_configs.h
    ${IAGENT_COMMON_DIR}/wa_alarm.h
)