
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.



#include "wa_edge_client_api.h"


bool wa_wagent_post_dp(WA_EDGE_CONTEXT context, const char * di, char * ri, char * payload, wa_format_type_t fmt)
{

    char uri[256];
    snprintf(uri, sizeof(uri), "/dp/%s/%s", di, ri);
    return wa_wagent_simple_request(context, COAP_POST, uri, NULL, payload, fmt);
}
