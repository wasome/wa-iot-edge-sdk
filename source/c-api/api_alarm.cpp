
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#include "common/wa_edge_configs.h"
#include "wa_edge_client_api.h"
#include "wa_alarm.h"
#include "wa_sdk.h"
#include "coap_convert.h"
#include "coap_sdk.h"
#include "wa_edge_client_api.h"

bool wa_post_alarm(WA_EDGE_CONTEXT context, iagent_alarm_t* alarm)
{
    char *payload = serialize_single_alarm(alarm);
    if (payload == NULL)  return false;

    int ret = wa_wagent_simple_request(context, COAP_POST, (char*) URI_ALARM, NULL, payload, IA_APPLICATION_JSON);
    free(payload);
    return ret;
}

bool wa_clear_alarm(WA_EDGE_CONTEXT context, alarm_id_t alarm_id, object_type_t target_type, const char * target_id, const char * clear_reason)
{
    bool ret = false;
    char * payload = clear_alarm_payload_A(
            alarm_id,
            target_type,
            target_id,
            clear_reason);
    if (payload == NULL)  return false;
    ret = wa_wagent_simple_request(context, COAP_POST, (char*)URI_ALARM, (char*)"action=clear", payload, IA_APPLICATION_JSON);
    free(payload);
    return ret;
}


bool wa_post_event(WA_EDGE_CONTEXT context, wa_event_t* event)
{
    bool ret = false;
    char *payload = serialize_single_event(event);
    if (payload == NULL) return false;
    ret = wa_wagent_simple_request(context, COAP_POST, (char*) URI_EVENT, NULL, payload, IA_APPLICATION_JSON);
    free(payload);
    return ret;    
}        


// the eject APIs post to local machine

bool wa_eject_alarm_set(iagent_alarm_t* alarm)
{
    char *payload = serialize_single_alarm(alarm);
    if(payload == NULL) return false;
    coap_packet_t packet[1];
    wa_setup_coap_request(packet, COAP_POST, (char*) URI_ALARM, NULL, payload, IA_APPLICATION_JSON);
    wa_coap_send_packet_IP(packet, "127.0.0.1", 5683);
    free(payload);
    return true;
}

bool wa_eject_alarm_clear(alarm_id_t alarm_id, object_type_t target_type, const char * target_id, const char * clear_reason)
{
    char * payload = clear_alarm_payload_A(
            alarm_id,
            target_type,
            target_id,
            clear_reason);
    if(payload == NULL) return false;
    coap_packet_t packet[1];
    wa_setup_coap_request(packet, COAP_POST, (char*)URI_ALARM, (char*)"action=clear", payload, IA_APPLICATION_JSON);
    wa_coap_send_packet_IP(packet, "127.0.0.1", 5683);
    free(payload);
    return true;
}



// don't wait for any response
bool wa_eject_event(wa_event_t* event)
{
    char *payload = serialize_single_event(event);
    if(payload == NULL) return false;
    coap_packet_t packet[1];
    wa_setup_coap_request(packet, COAP_POST, (char*) URI_EVENT, NULL, payload, IA_APPLICATION_JSON);
    wa_coap_send_packet_IP(packet, "127.0.0.1", 5683);
    free(payload);
    return true;
}        

bool wa_eject_event2(event_id_t event_id,
        severity_t severity,
        const char * title,
        object_type_t target_type,
        const char * target_id,
        const char * content)
{
    wa_event_t event[1];
    wa_init_event(event, event_id, severity, target_type, target_id, title, content);
    return wa_eject_event(event);
}