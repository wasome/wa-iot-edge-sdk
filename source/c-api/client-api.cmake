set(IAGENT_CLIENT_API_DIR ${CMAKE_CURRENT_LIST_DIR})

include_directories(${IAGENT_CLIENT_API_DIR})

include(${IAGENT_CLIENT_API_DIR}/../../external/hiredis.cmake)
include(${IAGENT_CLIENT_API_DIR}/../../external/util_deps.cmake)

set(IAGENT_CLIENT_API_SRC
    ${IAGENT_CLIENT_API_DIR}/api_alarm.cpp
    ${IAGENT_CLIENT_API_DIR}/wa_edge_client.cpp
    ${IAGENT_CLIENT_API_DIR}/api_dp.cpp
    ${IAGENT_CLIENT_API_DIR}/wa_edge_resources.cpp
    ${IAGENT_CLIENT_API_DIR}/wa_redis_client.cpp
    ${IAGENT_CLIENT_API_DIR}/wa_bus.cpp
    ${IAGENT_CLIENT_API_DIR}/wa_data_flow.cpp
)


set(IAGENT_CLIENT_API_SRC_FULL
    ${IDRM_SHARED_SOURCES}
    ${IAGENT_CLIENT_API_SRC}
    ${SOURCE_HIREDIS}
    ${SOURCE_UTIL_DEPS}
)


set(AGENT_CLIENT_API_INSTALL_HEADERS
    ${IAGENT_CLIENT_API_DIR}/wa_redis_api.h
    ${IAGENT_CLIENT_API_DIR}/wa_edge_client_api.h
    ${IAGENT_CLIENT_API_DIR}/wa_bus.h
)