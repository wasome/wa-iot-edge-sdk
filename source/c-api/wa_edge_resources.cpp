
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#include "wa_edge_client_api.h"

bool wa_wagent_query_info(WA_EDGE_CONTEXT context, iagent_info_t * info)
{
    bool ret = false;
    restful_response_t * ret_data = wa_wagent_request(context,
            COAP_GET,
            (char*)"ilink",
            NULL,
            (char*)"",
            IA_TEXT_PLAIN);

    if (ret_data != NULL)
    {
        // if the service has given response, we don't try to rerun the upload.
        if(ret_data->code == CONTENT_2_05 && ret_data->payload_len > 0)
        {
            ret = parse_iagent_info(ret_data->payload, ret_data->payload_len, info);
        }
        wa_free_response(ret_data);
    }

    return ret;
}

