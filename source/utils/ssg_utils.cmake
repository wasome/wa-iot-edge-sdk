set(SSG_UTILS_DIR ${CMAKE_CURRENT_LIST_DIR})

# required by the atomic.h in the duration_dist.cpp
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 ")


include_directories(${SSG_UTILS_DIR})

include(${SSG_UTILS_DIR}/../../external/util_deps.cmake)

set(SSG_UTILS_SOURCE
    ${SSG_UTILS_DIR}/string_parser.c
    ${SSG_UTILS_DIR}/url_util.c
    ${SSG_UTILS_DIR}/base64.cpp
    ${SSG_UTILS_DIR}/file_util.c
    ${SSG_UTILS_DIR}/duration_dist.cpp
    ${SSG_UTILS_DIR}/cJSON_ext.cpp
    ${SSG_UTILS_DIR}/modbus_coding.c
    ${SOURCE_UTIL_DEPS}
)


set(SSG_UTILS_SOURCE_FULL
    ${SSG_UTILS_DIR}/parson_ext.c
    ${EX_PARSON_DIR}/parson.c
    ${SSG_UTILS_SOURCE}
)


set(SOURCE_UTILS_SDK
    ${SSG_UTILS_DIR}/file_util.c
    ${SSG_UTILS_DIR}/../../external/md5c/md5.c
    )

set(UTILS_SDK_INSTALL_HEADERS
    ${SSG_UTILS_DIR}/parson_ext.h
    ${SSG_UTILS_DIR}/cJSON_ext.h
    ${SSG_UTILS_DIR}/duration_dist.h
    ${SSG_UTILS_DIR}/file_util.h
    ${SSG_UTILS_DIR}/modbus_coding.h
    ${SSG_UTILS_DIR}/string_parser.h
)
