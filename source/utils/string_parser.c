/*
 * Copyright (C) 2017 Intel Corporation.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "string_parser.h"



// define util functions for insert and find string from  a glogal list
struct _wa_tag
{
    char ** tags;
    int tags_num;
    int tags_size;
} ;


wa_tag_t* wa_tag_new(int init_size)
{
    wa_tag_t * tag_ctx = (wa_tag_t*)malloc(sizeof(wa_tag_t));
    if(tag_ctx == NULL) return NULL;
    memset(tag_ctx, 0, sizeof(wa_tag_t));
    if(init_size == 0) return tag_ctx;
    tag_ctx->tags_size = init_size;
    tag_ctx->tags = (char**)malloc(sizeof(char*) * init_size);
    if(tag_ctx->tags == NULL)
    {
        free(tag_ctx);
        return NULL;
    }
    memset(tag_ctx->tags, 0, sizeof(char*) * init_size);
    return tag_ctx;
}

void wa_tag_free(wa_tag_t * tag_ctx)
{
    if(tag_ctx == NULL) return;
    if(tag_ctx->tags)
    {
        for(int i = 0; i < tag_ctx->tags_num; i++)
        {
            if(tag_ctx->tags[i]) free(tag_ctx->tags[i]);
        }
        free(tag_ctx->tags);
    }
    free(tag_ctx);
}

const char * wa_tag_get(wa_tag_t * tag_ctx, int tag_idx)
{
    if(tag_idx < 0 || tag_idx >= tag_ctx->tags_num)
        return NULL;
    return tag_ctx->tags[tag_idx];
}

int wa_tag_find(wa_tag_t * tag_ctx, const char * tag)
{
    for(int i = 0; i < tag_ctx->tags_num; i++)
    {
        if(strcmp(tag_ctx->tags[i], tag) == 0)
            return i;
    }
    return -1;
}

int wa_tag_insert(wa_tag_t * tag_ctx, const char * tag)
{
    int idx = wa_tag_find(tag_ctx, tag);
    if(idx >= 0) return idx;
    if(tag_ctx->tags_num >= tag_ctx->tags_size)
    {
        int new_size = tag_ctx->tags_size + 10;
        char ** new_tags = (char**)malloc(sizeof(char*) * new_size);
        if(new_tags == NULL)
            return -1;
        memset(new_tags, 0, sizeof(char*) * new_size);
        if(tag_ctx->tags)
        {
            memcpy(new_tags, tag_ctx->tags, sizeof(char*) * tag_ctx->tags_num);
            free(tag_ctx->tags);
        }
        tag_ctx->tags = new_tags;
        tag_ctx->tags_size = new_size;
    }
    tag_ctx->tags[tag_ctx->tags_num] = strdup(tag);
    idx = tag_ctx->tags_num;
    tag_ctx->tags_num ++;
    return idx;
}


#if 0
/*
#include <map>
void parse_query_string(const char * query, std::map <std::string, std::string> & map_query)
{
	char * query_str = strdup(query);
	if(NULL == query_str)
	{
		return;
	}

	char * p = query_str;
	char * key = p;
	char * value = NULL;

    while(*p == ' ') p++;

	while(*p)
	{
		if(*p == '=')
		{
			*p = '\0';
			p++;
			while(*p == ' ') p++;
			value = p;
		}
		else if(*p == '&')
		{
			*p = '\0';
			if(value == NULL)
				goto end;

		    map_query[key] = value;
		    p++;
		    while(*p == ' ') p++;
		    value = NULL;
		    key = p;
		}
		else
		{
			p++;
		}
	}
end:
	free(query_str);
}

*/
#endif

// parse format [token1, token2] into a char * array which end with NULL pointer.
char ** parse_string_array(char * str, char delimiter)
{

	int cnt = 0;
	char ** l = NULL;
	char * value  = strdup(str);
	char * p = value;

	char * start;
	char * end;
    int i = 0;

	while(*p == ' ') p ++;
	if(*p != '[')
		goto end;
	p++;
	while(*p == ' ')p++;

	end = strrchr(p, ']');
	if(end == NULL) return NULL;
	*end = 0;
	if(*p == 0)goto end;

	start = p;
	while(*p)
	{
		if(*p == delimiter) cnt ++;
		p++;
	}

	l = (char **)malloc(sizeof(char **) * (cnt +2));
	memset(l, 0, sizeof(char **) * (cnt +2));
	p = value;
	while(*p)
	{
		if(*p == delimiter)
		{
			*p = 0;
			l[i++] = strdup(start);
			p++;
			while(*p == ' ')p++;
			start = p;
		}
		else
		{
			p++;
		}
	}
	l[i++] = strdup (start);
	assert(i == (cnt+1));

end:
	free (value);
	return l;
}


void free_str_list(char ** str_list)
{
	char ** p = str_list;
	while(*p)
	{
		free(*p);
		p++;
	}
	free(str_list);
}


