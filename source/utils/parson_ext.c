/*
 * Copyright (C) 2017 Intel Corporation.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "parson_ext.h"
#include <string.h>
#include <stdlib.h>

int get_json_number_safe(const JSON_Object *object, const char *name)
{
	JSON_Value * jvalue = json_object_get_value(object, name);
	if(jvalue && json_value_get_type(jvalue) == JSONNumber)
	   return (int) json_number(jvalue);

	return -1;
}

const char * get_json_string_safe(const JSON_Object *object, const char *name)
{
    JSON_Value * jvalue = json_object_get_value(object, name);
    if(jvalue && json_value_get_type(jvalue) == JSONString)
       return json_string(jvalue);

    return "";
}


JSON_Value * query_string_to_json(const char * query)
{
    if (NULL == query)
        return NULL;
    char * query_str = strdup(query);
    if(NULL == query_str)
    {
        return NULL;
    }

    JSON_Value *json_root = json_value_init_object();
    JSON_Object *  root_obj = json_object(json_root);

    char * p = query_str;
    while(*p == ' ') p++;
    char * key = p;
    char * value = NULL;

    while(*p)
    {
        if(*p == '=')
        {
            *p = '\0';
            p++;
            while(*p == ' ') p++;
            value = p;
        }
        else if(*p == '&')
        {
            *p = '\0';
            if(value == NULL)
                goto end;

            json_object_set_string(root_obj, key, value);
            p++;
            while(*p == ' ') p++;
            value = NULL;
            key = p;
        }
        else
        {
            p++;
        }
    }
    if (value != NULL && key != NULL)
        json_object_set_string(root_obj, key, value);
end:
    free(query_str);
    return json_root;
}


// parse format [token1, token2] into a char * array which end with NULL pointer.
JSON_Value * parse_string_array_to_Json(char * str, char delimiter)
{

    JSON_Value *json_root = NULL;
    char * value  = strdup(str);
    char * p = value;
    char * start;
    char * end;
    while(*p == ' ') p ++;
    if(*p != '[')
        goto end;
    p++;
    while(*p == ' ')p++;

    end = strrchr(p, ']');
    if(end == NULL) return NULL;
    *end = 0;
    if(*p == 0)goto end;

    json_root = json_value_init_array();
    start = p;
    p = value;
    while(*p)
    {
        if(*p == delimiter)
        {
            *p = 0;
            json_array_append_string(json_array(json_root), start);
            p++;
            while(*p == ' ')p++;
            start = p;
        }
        else
        {
            p++;
        }
    }
    if(start[0])
        json_array_append_string(json_array(json_root), start);

end:
    free (value);
    return json_root;
}
