
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#ifndef __MODBUS_CODING_h__
#define __MODBUS_CODING_h__

#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif


typedef enum {
    Coding_Invalid = -1,
    Coding_None = 0,
    Coding_F_abcd,
    Coding_F_cdab,
    Coding_F_dcba,
    Coding_F_badc,
    Coding_I_abcd,
    Coding_I_cdab,
    Coding_I_dcba,
    Coding_I_badc,
    Coding_Total_Size
} mb_coding_e;
extern const char * SZ_CODING[Coding_Total_Size];
#define MB_CODING_IS_FLOAT(code) ((code) >= Coding_F_abcd && (code) <= Coding_F_badc)
#define MB_CODING_IS_INT(code) ((code) >= Coding_I_abcd && (code) <= Coding_I_badc)

#define MB_CODING_IS_VALID(code) ((code) >= Coding_F_abcd && (code) <= Coding_I_badc)
mb_coding_e parse_modbus_coding(const char * code);
const char * modbus_coding_text(mb_coding_e coding);

typedef enum
{
	MB_Invalid_Reg_Type = -1,
	MB_Discrete = 0,
	MB_Coil,
	MB_Input,
	MB_Holding,
	MB_Max_Types
} mb_reg_type_e;

extern const char * SZ_MB_REG_TYPES[MB_Max_Types];
mb_reg_type_e check_mb_reg_type(const char* type_name);
const char * mb_reg_type_text(mb_reg_type_e t);


float my_modbus_get_float_abcd(const uint16_t *src, uint32_t * dest);

/* Get a float from 4 bytes (Modbus) with swapped words (CDAB) */
float my_modbus_get_float_cdab(const uint16_t *src, uint32_t * dest);

/* Get a float from 4 bytes (Modbus) in inversed format (DCBA) */
float my_modbus_get_float_dcba(const uint16_t *src, uint32_t * dest);

/* Get a float from 4 bytes (Modbus) with swapped bytes (BADC) */
float my_modbus_get_float_badc(const uint16_t *src, uint32_t * dest);


void my_modbus_set_abcd(uint32_t * p, uint16_t *dest);

void my_modbus_set_dcba(uint32_t * p, uint16_t *dest);

void my_modbus_set_badc(uint32_t * p, uint16_t *dest);

void my_modbus_set_cdab(uint32_t * p, uint16_t *dest);



#ifdef __cplusplus
}
#endif

#endif