/*
 * iagent_utils.h
 *
 *  Created on: Feb 24, 2018
 *      Author: xin
 */

#ifndef IDR_MGT_SHARED_LIBS_UTILS_IAGENT_UTILS_H_
#define IDR_MGT_SHARED_LIBS_UTILS_IAGENT_UTILS_H_


#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define DEFAULT_IAGENT_STORE_PATH "/etc/ams/iagent_store"
#define DEFAULT_SN_FILE_NAME "/etc/ams/iagent_store/sn.bin"
#define DEFAULT_PROV_KEY_FILE_NAME "/etc/ams/iagent_store/prov_key.bin"
#define DEFAULT_DI_FILE_NAME "/etc/ams/iagent_store/device_id"
#define DEFAULT_FACTORY_KEY_FILE_NAME "/etc/ams/iagent_store/factory.key"
#define DEFAULT_PROV_STATUS_FILE_NAME "/etc/ams/iagent_store/provistion_status"


bool getMAC_sysfile(char * mac_buffer, int buffer_len);
bool is_valid_mac(char * mac);
bool gen_MAC_file_ifconfig(char * filepath);
bool mac_eth0(char * mac_buffer, int buffer_len);

bool check_hw_serials_file(char * path, char * serials_buffer, int buffer_len);

bool load_device_id(char * id, int len);
bool save_device_id(char *id);
bool load_factory_key( char **name, char **key);

#ifdef __cplusplus
}
#endif

#endif /* IDR_MGT_SHARED_LIBS_UTILS_IAGENT_UTILS_H_ */
