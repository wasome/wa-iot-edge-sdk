
// Copyright (c) WanSheng Intelligent Corp. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.


#ifndef __WASDK_CONSTS_H_
#define __WASDK_CONSTS_H_

/* Note: this header file is used only by the library source files internally! */

/* defined for the library mask=LOG_WAGENT_SDK_MASK_ID(9) */

#define LOG_FLAG_MISC           0x00000002
#define LOG_FLAG_RESTFUL        0x00000004
#define LOG_FLAG_TRANSACTION    0x00000100
#define LOG_FLAG_TASK           0x00000200
#define LOG_FLAG_COAP_REST      0x00000400


#define FLAG_LIB_DEBUG_TASK     0x10000000
#define LOG_FLAG_COAP_RAW       0x20000000
#define LOG_FLAG_DEBUG_TRANS    0x40000000

#ifdef LOG_MY_MASK_ID
#undef LOG_MY_MASK_ID
#endif
// refer to LOG_WAGENT_SDK_MASK_ID
#define LOG_MY_MASK_ID     9


#endif