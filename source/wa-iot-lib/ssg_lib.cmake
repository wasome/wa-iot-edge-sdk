set(SSG_LIB_DIR ${CMAKE_CURRENT_LIST_DIR})

include_directories(${SSG_LIB_DIR}
    ${SSG_LIB_DIR}/iniparser/src
)

set(SSG_LIB_SOURCE
    ${SSG_LIB_DIR}/message_queue.c
    ${SSG_LIB_DIR}/transaction.c
    ${SSG_LIB_DIR}/linux/sync_bsp.c
    ${SSG_LIB_DIR}/logs.c
    ${SSG_LIB_DIR}/url_match.c
    ${SSG_LIB_DIR}/task.c
    ${SSG_LIB_DIR}/path_util.c    
    ${SSG_LIB_DIR}/misc.cpp    
    ${SSG_LIB_DIR}/hw_serials_num.c
    ${SSG_LIB_DIR}/iniparser/src/iniparser.c
    ${SSG_LIB_DIR}/iniparser/src/dictionary.c
    
)

set(SSG_LIB_INSTALL_HEADERS
    ${SSG_LIB_DIR}/misc.h
    ${SSG_LIB_DIR}/agent_core_lib.h
    ${SSG_LIB_DIR}/hw_serials_num.h
    ${SSG_LIB_DIR}/logs.h
    ${SSG_LIB_DIR}/iagent_bsp.h
    ${SSG_LIB_DIR}/iagent_bsp_linux.h
    ${SSG_LIB_DIR}/iniparser/src/iniparser.h
    ${SSG_LIB_DIR}/iniparser/src/dictionary.h
)
